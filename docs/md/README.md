
TIC Makers - React Native Dialog
================================

React native component for dialog.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Dialog Expo's snack

Install
-------

Install `@ticmakers-react-native/dialog` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/dialog --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/dialog
```

How to use?
-----------

```javascript
import React from 'react'
import Dialog from '@ticmakers-react-native/dialog'

export default class App extends React.Component {

  render() { }
}
```

Properties
----------

Name

Type

Default Value

Definition

\----

\----

\----

\----

Todo
----

*   Test on iOS
*   Improve and add new features
*   Add more styles
*   Create tests
*   Add new props and components in readme
*   Improve README

Version 1.0.2 ([Changelog](https://bitbucket.org/ticmakers/rn-dialog/src/master/CHANGELOG.md))
----------------------------------------------------------------------------------------------

## Index

### External modules

* ["DialogButton/DialogButton"](modules/_dialogbutton_dialogbutton_.md)
* ["DialogButton/index.d"](modules/_dialogbutton_index_d_.md)
* ["DialogButton/styles"](modules/_dialogbutton_styles_.md)
* ["DialogComponent/Dialog"](modules/_dialogcomponent_dialog_.md)
* ["DialogComponent/index.d"](modules/_dialogcomponent_index_d_.md)
* ["DialogComponent/styles"](modules/_dialogcomponent_styles_.md)
* ["DialogContent/DialogContent"](modules/_dialogcontent_dialogcontent_.md)
* ["DialogContent/index.d"](modules/_dialogcontent_index_d_.md)
* ["DialogContent/styles"](modules/_dialogcontent_styles_.md)
* ["DialogFooter/DialogFooter"](modules/_dialogfooter_dialogfooter_.md)
* ["DialogFooter/index.d"](modules/_dialogfooter_index_d_.md)
* ["DialogFooter/styles"](modules/_dialogfooter_styles_.md)
* ["DialogManager/DialogManager"](modules/_dialogmanager_dialogmanager_.md)
* ["DialogManager/index.d"](modules/_dialogmanager_index_d_.md)
* ["DialogTitle/DialogTitle"](modules/_dialogtitle_dialogtitle_.md)
* ["DialogTitle/index.d"](modules/_dialogtitle_index_d_.md)
* ["DialogTitle/styles"](modules/_dialogtitle_styles_.md)
* ["index"](modules/_index_.md)

---


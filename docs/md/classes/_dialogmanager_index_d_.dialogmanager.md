[@ticmakers-react-native/dialog](../README.md) > ["DialogManager/index.d"](../modules/_dialogmanager_index_d_.md) > [DialogManager](../classes/_dialogmanager_index_d_.dialogmanager.md)

# Class: DialogManager

Class to define the dialog manager component

*__class__*: DialogManager

## Hierarchy

**DialogManager**

## Index

### Properties

* [currentDialog](_dialogmanager_index_d_.dialogmanager.md#currentdialog)
* [destroyTimeout](_dialogmanager_index_d_.dialogmanager.md#destroytimeout)
* [dialogs](_dialogmanager_index_d_.dialogmanager.md#dialogs)

### Methods

* [add](_dialogmanager_index_d_.dialogmanager.md#add)
* [destroy](_dialogmanager_index_d_.dialogmanager.md#destroy)
* [dismiss](_dialogmanager_index_d_.dialogmanager.md#dismiss)
* [dismissAll](_dialogmanager_index_d_.dialogmanager.md#dismissall)
* [onDialogDismissed](_dialogmanager_index_d_.dialogmanager.md#ondialogdismissed)
* [show](_dialogmanager_index_d_.dialogmanager.md#show)
* [update](_dialogmanager_index_d_.dialogmanager.md#update)

---

## Properties

<a id="currentdialog"></a>

###  currentDialog

**● currentDialog**: *`RootSiblings`*

*Defined in DialogManager/index.d.ts:30*

Method to get the current dialog

*__readonly__*: 

*__type__*: {RootSiblings}

*__memberof__*: DialogManager

___
<a id="destroytimeout"></a>

###  destroyTimeout

**● destroyTimeout**: *`number`*

*Defined in DialogManager/index.d.ts:15*

A number millisecond to define the time to destroy the dialog list components

*__type__*: {number}

*__memberof__*: DialogManager

*__default__*: 150

___
<a id="dialogs"></a>

###  dialogs

**● dialogs**: *`RootSiblings`[]*

*Defined in DialogManager/index.d.ts:22*

A list of dialogs to show

*__type__*: {RootSiblings\[\]}

*__memberof__*: DialogManager

___

## Methods

<a id="add"></a>

###  add

▸ **add**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/index.d.ts:38*

Method to add a new dialog to the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |  A object of the DialogProps |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="destroy"></a>

###  destroy

▸ **destroy**(): `void`

*Defined in DialogManager/index.d.ts:44*

Method to destroy a dialog of the list

*__memberof__*: DialogManager

**Returns:** `void`

___
<a id="dismiss"></a>

###  dismiss

▸ **dismiss**(callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/index.d.ts:74*

Method to dismiss the dialog of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="dismissall"></a>

###  dismissAll

▸ **dismissAll**(callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/index.d.ts:81*

Method to dismiss all the dialogs of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="ondialogdismissed"></a>

###  onDialogDismissed

▸ **onDialogDismissed**(onDismiss?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/index.d.ts:51*

Method that fire when the a dialog of the list is dismissed

*__memberof__*: DialogManager

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` onDismiss | `undefined` \| `function` |

**Returns:** `void`

___
<a id="show"></a>

###  show

▸ **show**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/index.d.ts:67*

Method to show the dialog of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |  An object of the DialogProps |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="update"></a>

###  update

▸ **update**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/index.d.ts:59*

Method to update a dialog of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |  An object of the DialogProps |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___


[@ticmakers-react-native/dialog](../README.md) > ["DialogComponent/index.d"](../modules/_dialogcomponent_index_d_.md) > [Dialog](../classes/_dialogcomponent_index_d_.dialog.md)

# Class: Dialog

Class to define the Dialog component

*__class__*: Dialog

*__extends__*: {React.Component<IDialogProps, IDialogState>}

## Type parameters
#### SS 
## Hierarchy

 `Component`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md), [IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>

**↳ Dialog**

## Index

### Constructors

* [constructor](_dialogcomponent_index_d_.dialog.md#constructor)

### Properties

* [context](_dialogcomponent_index_d_.dialog.md#context)
* [props](_dialogcomponent_index_d_.dialog.md#props)
* [refs](_dialogcomponent_index_d_.dialog.md#refs)
* [root](_dialogcomponent_index_d_.dialog.md#root)
* [state](_dialogcomponent_index_d_.dialog.md#state)
* [contextType](_dialogcomponent_index_d_.dialog.md#contexttype)

### Methods

* [Title](_dialogcomponent_index_d_.dialog.md#title)
* [UNSAFE_componentWillMount](_dialogcomponent_index_d_.dialog.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_dialogcomponent_index_d_.dialog.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_dialogcomponent_index_d_.dialog.md#unsafe_componentwillupdate)
* [_getAnimation](_dialogcomponent_index_d_.dialog.md#_getanimation)
* [_onDismiss](_dialogcomponent_index_d_.dialog.md#_ondismiss)
* [_onShow](_dialogcomponent_index_d_.dialog.md#_onshow)
* [_processProps](_dialogcomponent_index_d_.dialog.md#_processprops)
* [componentDidCatch](_dialogcomponent_index_d_.dialog.md#componentdidcatch)
* [componentDidMount](_dialogcomponent_index_d_.dialog.md#componentdidmount)
* [componentDidUpdate](_dialogcomponent_index_d_.dialog.md#componentdidupdate)
* [componentWillMount](_dialogcomponent_index_d_.dialog.md#componentwillmount)
* [componentWillReceiveProps](_dialogcomponent_index_d_.dialog.md#componentwillreceiveprops)
* [componentWillUnmount](_dialogcomponent_index_d_.dialog.md#componentwillunmount)
* [componentWillUpdate](_dialogcomponent_index_d_.dialog.md#componentwillupdate)
* [dismiss](_dialogcomponent_index_d_.dialog.md#dismiss)
* [forceUpdate](_dialogcomponent_index_d_.dialog.md#forceupdate)
* [getSnapshotBeforeUpdate](_dialogcomponent_index_d_.dialog.md#getsnapshotbeforeupdate)
* [render](_dialogcomponent_index_d_.dialog.md#render)
* [setState](_dialogcomponent_index_d_.dialog.md#setstate)
* [shouldComponentUpdate](_dialogcomponent_index_d_.dialog.md#shouldcomponentupdate)
* [show](_dialogcomponent_index_d_.dialog.md#show)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new Dialog**(props: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*): [Dialog](_dialogcomponent_index_d_.dialog.md)

⊕ **new Dialog**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, context?: *`any`*): [Dialog](_dialogcomponent_index_d_.dialog.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:428*

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |

**Returns:** [Dialog](_dialogcomponent_index_d_.dialog.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:430*

*__deprecated__*: 

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |
| `Optional` context | `any` |

**Returns:** [Dialog](_dialogcomponent_index_d_.dialog.md)

___

## Properties

<a id="context"></a>

###  context

**● context**: *`any`*

*Inherited from Component.context*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:428*

If using the new style context, re-declare this in your class to be the `React.ContextType` of your `static contextType`.

```ts
static contextType = MyContext
// For TS pre-3.7:
context!: React.ContextType<typeof MyContext>
// For TS 3.7 and above:
declare context: React.ContextType<typeof MyContext>
```

*__deprecated__*: if used without a type annotation, or without static contextType

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

___
<a id="props"></a>

###  props

**● props**: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> & `Readonly`<`object`>*

*Inherited from Component.props*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:453*

___
<a id="refs"></a>

###  refs

**● refs**: *`object`*

*Inherited from Component.refs*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:459*

*__deprecated__*: [https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs](https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs)

#### Type declaration

[key: `string`]: `ReactInstance`

___
<a id="root"></a>

### `<Optional>` root

**● root**: *`PopupDialog`*

*Defined in DialogComponent/index.d.ts:205*

Reference of the PopupDialog

*__type__*: {PopupDialog}

*__memberof__*: Dialog

___
<a id="state"></a>

###  state

**● state**: *`Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>*

*Inherited from Component.state*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:454*

___
<a id="contexttype"></a>

### `<Static>``<Optional>` contextType

**● contextType**: *`Context`<`any`>*

*Inherited from Component.contextType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:410*

If set, `this.context` will be set at runtime to the current value of the given Context.

Usage:

```ts
type MyContext = number
const Ctx = React.createContext<MyContext>(0)

class Foo extends React.Component {
  static contextType = Ctx
  context!: React.ContextType<typeof Ctx>
  render () {
    return <>My context's value: {this.context}</>;
  }
}
```

*__see__*: [https://reactjs.org/docs/context.html#classcontexttype](https://reactjs.org/docs/context.html#classcontexttype)

___

## Methods

<a id="title"></a>

###  Title

▸ **Title**(): `TypeComponent`

*Defined in DialogComponent/index.d.ts:219*

Method that renders the Title component

*__memberof__*: Dialog

**Returns:** `TypeComponent`

___
<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:641*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:673*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, nextState: *`Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:701*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| nextState | `Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="_getanimation"></a>

### `<Private>` _getAnimation

▸ **_getAnimation**(): `FadeAnimation` \| `ScaleAnimation` \| `SlideAnimation` \| `undefined`

*Defined in DialogComponent/index.d.ts:257*

Method to get the animation when the dialog is showed

*__memberof__*: Dialog

**Returns:** `FadeAnimation` \| `ScaleAnimation` \| `SlideAnimation` \| `undefined`

___
<a id="_ondismiss"></a>

### `<Private>` _onDismiss

▸ **_onDismiss**(): `void`

*Defined in DialogComponent/index.d.ts:241*

Method that fire when the dialog is dismissed

*__memberof__*: Dialog

**Returns:** `void`

___
<a id="_onshow"></a>

### `<Private>` _onShow

▸ **_onShow**(): `void`

*Defined in DialogComponent/index.d.ts:249*

Method that fire when the dialog is showed

*__memberof__*: Dialog

**Returns:** `void`

___
<a id="_processprops"></a>

### `<Private>` _processProps

▸ **_processProps**(): [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)

*Defined in DialogComponent/index.d.ts:265*

Method to process the props

*__memberof__*: Dialog

**Returns:** [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:570*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:549*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, prevState: *`Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:612*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| prevState | `Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:627*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:656*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:565*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, nextState: *`Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:686*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| nextState | `Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="dismiss"></a>

###  dismiss

▸ **dismiss**(): `void`

*Defined in DialogComponent/index.d.ts:233*

Method to dismiss the dialog

*__memberof__*: Dialog

**Returns:** `void`

___
<a id="forceupdate"></a>

###  forceUpdate

▸ **forceUpdate**(callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.forceUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:445*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, prevState: *`Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:606*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| prevState | `Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)> |

**Returns:** `SS` \| `null`

___
<a id="render"></a>

###  render

▸ **render**(): `TypeComponent`

*Overrides Component.render*

*Defined in DialogComponent/index.d.ts:212*

Method that renders the component

*__memberof__*: Dialog

**Returns:** `TypeComponent`

___
<a id="setstate"></a>

###  setState

▸ **setState**<`K`>(state: *`function` \| `null` \| `S` \| `object`*, callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.setState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:440*

**Type parameters:**

#### K :  `keyof IDialogState`
**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `function` \| `null` \| `S` \| `object` |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)>*, nextState: *`Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:560*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)> |
| nextState | `Readonly`<[IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)> |
| nextContext | `any` |

**Returns:** `boolean`

___
<a id="show"></a>

###  show

▸ **show**(): `void`

*Defined in DialogComponent/index.d.ts:226*

Method to show the dialog

*__memberof__*: Dialog

**Returns:** `void`

___


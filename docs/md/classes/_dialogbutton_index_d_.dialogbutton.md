[@ticmakers-react-native/dialog](../README.md) > ["DialogButton/index.d"](../modules/_dialogbutton_index_d_.md) > [DialogButton](../classes/_dialogbutton_index_d_.dialogbutton.md)

# Class: DialogButton

Class to define the component DialogButton used in Dialog

*__class__*: DialogButton

*__extends__*: {React.Component<IDialogButtonProps, IDialogButtonState>}

## Type parameters
#### SS 
## Hierarchy

 `Component`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md), [IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>

**↳ DialogButton**

## Index

### Constructors

* [constructor](_dialogbutton_index_d_.dialogbutton.md#constructor)

### Properties

* [context](_dialogbutton_index_d_.dialogbutton.md#context)
* [props](_dialogbutton_index_d_.dialogbutton.md#props)
* [refs](_dialogbutton_index_d_.dialogbutton.md#refs)
* [state](_dialogbutton_index_d_.dialogbutton.md#state)
* [contextType](_dialogbutton_index_d_.dialogbutton.md#contexttype)

### Methods

* [UNSAFE_componentWillMount](_dialogbutton_index_d_.dialogbutton.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_dialogbutton_index_d_.dialogbutton.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_dialogbutton_index_d_.dialogbutton.md#unsafe_componentwillupdate)
* [_onPress](_dialogbutton_index_d_.dialogbutton.md#_onpress)
* [_processProps](_dialogbutton_index_d_.dialogbutton.md#_processprops)
* [componentDidCatch](_dialogbutton_index_d_.dialogbutton.md#componentdidcatch)
* [componentDidMount](_dialogbutton_index_d_.dialogbutton.md#componentdidmount)
* [componentDidUpdate](_dialogbutton_index_d_.dialogbutton.md#componentdidupdate)
* [componentWillMount](_dialogbutton_index_d_.dialogbutton.md#componentwillmount)
* [componentWillReceiveProps](_dialogbutton_index_d_.dialogbutton.md#componentwillreceiveprops)
* [componentWillUnmount](_dialogbutton_index_d_.dialogbutton.md#componentwillunmount)
* [componentWillUpdate](_dialogbutton_index_d_.dialogbutton.md#componentwillupdate)
* [forceUpdate](_dialogbutton_index_d_.dialogbutton.md#forceupdate)
* [getSnapshotBeforeUpdate](_dialogbutton_index_d_.dialogbutton.md#getsnapshotbeforeupdate)
* [render](_dialogbutton_index_d_.dialogbutton.md#render)
* [setState](_dialogbutton_index_d_.dialogbutton.md#setstate)
* [shouldComponentUpdate](_dialogbutton_index_d_.dialogbutton.md#shouldcomponentupdate)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new DialogButton**(props: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*): [DialogButton](_dialogbutton_index_d_.dialogbutton.md)

⊕ **new DialogButton**(props: *[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)*, context?: *`any`*): [DialogButton](_dialogbutton_index_d_.dialogbutton.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:428*

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |

**Returns:** [DialogButton](_dialogbutton_index_d_.dialogbutton.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:430*

*__deprecated__*: 

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | [IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md) |
| `Optional` context | `any` |

**Returns:** [DialogButton](_dialogbutton_index_d_.dialogbutton.md)

___

## Properties

<a id="context"></a>

###  context

**● context**: *`any`*

*Inherited from Component.context*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:428*

If using the new style context, re-declare this in your class to be the `React.ContextType` of your `static contextType`.

```ts
static contextType = MyContext
// For TS pre-3.7:
context!: React.ContextType<typeof MyContext>
// For TS 3.7 and above:
declare context: React.ContextType<typeof MyContext>
```

*__deprecated__*: if used without a type annotation, or without static contextType

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

___
<a id="props"></a>

###  props

**● props**: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> & `Readonly`<`object`>*

*Inherited from Component.props*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:453*

___
<a id="refs"></a>

###  refs

**● refs**: *`object`*

*Inherited from Component.refs*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:459*

*__deprecated__*: [https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs](https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs)

#### Type declaration

[key: `string`]: `ReactInstance`

___
<a id="state"></a>

###  state

**● state**: *`Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>*

*Inherited from Component.state*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:454*

___
<a id="contexttype"></a>

### `<Static>``<Optional>` contextType

**● contextType**: *`Context`<`any`>*

*Inherited from Component.contextType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:410*

If set, `this.context` will be set at runtime to the current value of the given Context.

Usage:

```ts
type MyContext = number
const Ctx = React.createContext<MyContext>(0)

class Foo extends React.Component {
  static contextType = Ctx
  context!: React.ContextType<typeof Ctx>
  render () {
    return <>My context's value: {this.context}</>;
  }
}
```

*__see__*: [https://reactjs.org/docs/context.html#classcontexttype](https://reactjs.org/docs/context.html#classcontexttype)

___

## Methods

<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:641*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:673*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, nextState: *`Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:701*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| nextState | `Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="_onpress"></a>

### `<Private>` _onPress

▸ **_onPress**(): `void`

*Defined in DialogButton/index.d.ts:105*

Method that fire when the button is pressed

*__memberof__*: DialogButton

**Returns:** `void`

___
<a id="_processprops"></a>

### `<Private>` _processProps

▸ **_processProps**(): [IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)

*Defined in DialogButton/index.d.ts:113*

Method that process the props

*__memberof__*: DialogButton

**Returns:** [IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:570*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:549*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, prevState: *`Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:612*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| prevState | `Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:627*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:656*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:565*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, nextState: *`Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:686*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| nextState | `Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="forceupdate"></a>

###  forceUpdate

▸ **forceUpdate**(callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.forceUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:445*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, prevState: *`Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:606*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| prevState | `Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)> |

**Returns:** `SS` \| `null`

___
<a id="render"></a>

###  render

▸ **render**(): `TypeComponent`

*Overrides Component.render*

*Defined in DialogButton/index.d.ts:97*

Method that renders the component

*__memberof__*: DialogButton

**Returns:** `TypeComponent`

___
<a id="setstate"></a>

###  setState

▸ **setState**<`K`>(state: *`function` \| `null` \| `S` \| `object`*, callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.setState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:440*

**Type parameters:**

#### K :  `keyof IDialogButtonState`
**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `function` \| `null` \| `S` \| `object` |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)>*, nextState: *`Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Dialog/node_modules/@types/react/index.d.ts:560*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)> |
| nextState | `Readonly`<[IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)> |
| nextContext | `any` |

**Returns:** `boolean`

___


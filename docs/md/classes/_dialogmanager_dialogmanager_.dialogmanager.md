[@ticmakers-react-native/dialog](../README.md) > ["DialogManager/DialogManager"](../modules/_dialogmanager_dialogmanager_.md) > [DialogManager](../classes/_dialogmanager_dialogmanager_.dialogmanager.md)

# Class: DialogManager

Class to define the dialog manager component

*__class__*: DialogManager

## Hierarchy

**DialogManager**

## Index

### Constructors

* [constructor](_dialogmanager_dialogmanager_.dialogmanager.md#constructor)

### Properties

* [destroyTimeout](_dialogmanager_dialogmanager_.dialogmanager.md#destroytimeout)
* [dialogs](_dialogmanager_dialogmanager_.dialogmanager.md#dialogs)

### Accessors

* [currentDialog](_dialogmanager_dialogmanager_.dialogmanager.md#currentdialog)

### Methods

* [add](_dialogmanager_dialogmanager_.dialogmanager.md#add)
* [destroy](_dialogmanager_dialogmanager_.dialogmanager.md#destroy)
* [dismiss](_dialogmanager_dialogmanager_.dialogmanager.md#dismiss)
* [dismissAll](_dialogmanager_dialogmanager_.dialogmanager.md#dismissall)
* [onDialogDismissed](_dialogmanager_dialogmanager_.dialogmanager.md#ondialogdismissed)
* [show](_dialogmanager_dialogmanager_.dialogmanager.md#show)
* [update](_dialogmanager_dialogmanager_.dialogmanager.md#update)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new DialogManager**(): [DialogManager](_dialogmanager_dialogmanager_.dialogmanager.md)

*Defined in DialogManager/DialogManager.tsx:25*

Creates an instance of DialogManager.

*__memberof__*: DialogManager

**Returns:** [DialogManager](_dialogmanager_dialogmanager_.dialogmanager.md)

___

## Properties

<a id="destroytimeout"></a>

###  destroyTimeout

**● destroyTimeout**: *`number`*

*Defined in DialogManager/DialogManager.tsx:18*

A number millisecond to define the time to destroy the dialog list components

*__type__*: {number}

*__memberof__*: DialogManager

*__default__*: 150

___
<a id="dialogs"></a>

###  dialogs

**● dialogs**: *`RootSiblings`[]*

*Defined in DialogManager/DialogManager.tsx:25*

A list of dialogs to show

*__type__*: {RootSiblings\[\]}

*__memberof__*: DialogManager

___

## Accessors

<a id="currentdialog"></a>

###  currentDialog

**get currentDialog**(): `RootSiblings`

*Defined in DialogManager/DialogManager.tsx:42*

Method to get the current dialog

*__readonly__*: 

*__type__*: {RootSiblings}

*__memberof__*: DialogManager

**Returns:** `RootSiblings`

___

## Methods

<a id="add"></a>

###  add

▸ **add**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/DialogManager.tsx:52*

Method to add a new dialog to the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |  A object of the DialogProps |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="destroy"></a>

###  destroy

▸ **destroy**(): `void`

*Defined in DialogManager/DialogManager.tsx:70*

Method to destroy a dialog of the list

*__memberof__*: DialogManager

**Returns:** `void`

___
<a id="dismiss"></a>

###  dismiss

▸ **dismiss**(callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/DialogManager.tsx:118*

Method to dismiss the dialog of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="dismissall"></a>

###  dismissAll

▸ **dismissAll**(callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/DialogManager.tsx:127*

Method to dismiss all the dialogs of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="ondialogdismissed"></a>

###  onDialogDismissed

▸ **onDialogDismissed**(onDismiss?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/DialogManager.tsx:80*

Method that fire when the a dialog of the list is dismissed

*__memberof__*: DialogManager

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` onDismiss | `undefined` \| `function` |

**Returns:** `void`

___
<a id="show"></a>

###  show

▸ **show**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/DialogManager.tsx:109*

Method to show the dialog of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |  An object of the DialogProps |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="update"></a>

###  update

▸ **update**(props: *[IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)*, callback?: *`undefined` \| `function`*): `void`

*Defined in DialogManager/DialogManager.tsx:91*

Method to update a dialog of the list

*__memberof__*: DialogManager

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md) |  An object of the DialogProps |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___


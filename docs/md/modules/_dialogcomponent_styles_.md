[@ticmakers-react-native/dialog](../README.md) > ["DialogComponent/styles"](../modules/_dialogcomponent_styles_.md)

# External module: "DialogComponent/styles"

## Index

### Variables

* [styles](_dialogcomponent_styles_.md#styles)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    borderRadius: 0,
    elevation: 5,
    minHeight: 96,
    shadowColor: 'black',
    shadowOffset: {
      height: 2,
      width: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },

  title: {
  },
  titleContainer: {
  },
})

*Defined in DialogComponent/styles.ts:3*

#### Type declaration

 title: `object`

 titleContainer: `object`

 container: `object`

 borderRadius: `number`

 elevation: `number`

 minHeight: `number`

 shadowColor: `string`

 shadowOpacity: `number`

 shadowRadius: `number`

 shadowOffset: `object`

 height: `number`

 width: `number`

___


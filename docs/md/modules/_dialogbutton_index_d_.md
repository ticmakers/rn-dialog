[@ticmakers-react-native/dialog](../README.md) > ["DialogButton/index.d"](../modules/_dialogbutton_index_d_.md)

# External module: "DialogButton/index.d"

## Index

### Classes

* [DialogButton](../classes/_dialogbutton_index_d_.dialogbutton.md)

### Interfaces

* [IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)
* [IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)

### Type aliases

* [TypeDialogButtonAlign](_dialogbutton_index_d_.md#typedialogbuttonalign)

---

## Type aliases

<a id="typedialogbuttonalign"></a>

###  TypeDialogButtonAlign

**Ƭ TypeDialogButtonAlign**: *"left" \| "center" \| "right"*

*Defined in DialogButton/index.d.ts:8*

Type to define the align position for the DialogButton component

___


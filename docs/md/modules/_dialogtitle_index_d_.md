[@ticmakers-react-native/dialog](../README.md) > ["DialogTitle/index.d"](../modules/_dialogtitle_index_d_.md)

# External module: "DialogTitle/index.d"

## Index

### Classes

* [DialogTitle](../classes/_dialogtitle_index_d_.dialogtitle.md)

### Interfaces

* [IDialogTitleProps](../interfaces/_dialogtitle_index_d_.idialogtitleprops.md)
* [IDialogTitleState](../interfaces/_dialogtitle_index_d_.idialogtitlestate.md)

### Type aliases

* [TypeDialogTitleAlign](_dialogtitle_index_d_.md#typedialogtitlealign)

---

## Type aliases

<a id="typedialogtitlealign"></a>

###  TypeDialogTitleAlign

**Ƭ TypeDialogTitleAlign**: *"left" \| "center" \| "right"*

*Defined in DialogTitle/index.d.ts:8*

Type to define the align position for the DialogTitle component

___


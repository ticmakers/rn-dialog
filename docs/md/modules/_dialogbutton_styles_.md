[@ticmakers-react-native/dialog](../README.md) > ["DialogButton/styles"](../modules/_dialogbutton_styles_.md)

# External module: "DialogButton/styles"

## Index

### Variables

* [styles](_dialogbutton_styles_.md#styles)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    padding: 20,
  },

  title: {
  },
})

*Defined in DialogButton/styles.ts:3*

#### Type declaration

 title: `object`

 container: `object`

 padding: `number`

___


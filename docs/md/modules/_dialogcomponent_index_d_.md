[@ticmakers-react-native/dialog](../README.md) > ["DialogComponent/index.d"](../modules/_dialogcomponent_index_d_.md)

# External module: "DialogComponent/index.d"

## Index

### Classes

* [Dialog](../classes/_dialogcomponent_index_d_.dialog.md)

### Interfaces

* [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)
* [IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)

### Type aliases

* [TypeDialogAnimation](_dialogcomponent_index_d_.md#typedialoganimation)
* [TypeDialogSlideFrom](_dialogcomponent_index_d_.md#typedialogslidefrom)

---

## Type aliases

<a id="typedialoganimation"></a>

###  TypeDialogAnimation

**Ƭ TypeDialogAnimation**: *"fade" \| "scale" \| "slide"*

*Defined in DialogComponent/index.d.ts:9*

Typing to define the type animation for the Dialog component

___
<a id="typedialogslidefrom"></a>

###  TypeDialogSlideFrom

**Ƭ TypeDialogSlideFrom**: *"top" \| "bottom" \| "left" \| "right"*

*Defined in DialogComponent/index.d.ts:14*

Typing to define the type animation for the slide animation

___


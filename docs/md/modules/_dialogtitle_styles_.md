[@ticmakers-react-native/dialog](../README.md) > ["DialogTitle/styles"](../modules/_dialogtitle_styles_.md)

# External module: "DialogTitle/styles"

## Index

### Variables

* [scale](_dialogtitle_styles_.md#scale)
* [styles](_dialogtitle_styles_.md#styles)

### Functions

* [normalize](_dialogtitle_styles_.md#normalize)

---

## Variables

<a id="scale"></a>

### `<Const>` scale

**● scale**: *`number`* =  Dimensions.get('window').width / 375

*Defined in DialogTitle/styles.ts:3*

___
<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    padding: 20,
  },

  title: {
    fontSize: normalize(17),
    lineHeight: normalize(23),
  },
})

*Defined in DialogTitle/styles.ts:9*

#### Type declaration

 container: `object`

 padding: `number`

 title: `object`

 fontSize: `number`

 lineHeight: `number`

___

## Functions

<a id="normalize"></a>

### `<Const>` normalize

▸ **normalize**(size: *`number`*): `number`

*Defined in DialogTitle/styles.ts:5*

**Parameters:**

| Name | Type |
| ------ | ------ |
| size | `number` |

**Returns:** `number`

___


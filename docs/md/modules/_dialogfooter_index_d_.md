[@ticmakers-react-native/dialog](../README.md) > ["DialogFooter/index.d"](../modules/_dialogfooter_index_d_.md)

# External module: "DialogFooter/index.d"

## Index

### Classes

* [DialogFooter](../classes/_dialogfooter_index_d_.dialogfooter.md)

### Interfaces

* [IDialogFooterProps](../interfaces/_dialogfooter_index_d_.idialogfooterprops.md)
* [IDialogFooterState](../interfaces/_dialogfooter_index_d_.idialogfooterstate.md)

---


[@ticmakers-react-native/dialog](../README.md) > ["DialogContent/styles"](../modules/_dialogcontent_styles_.md)

# External module: "DialogContent/styles"

## Index

### Variables

* [styles](_dialogcontent_styles_.md#styles)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
    padding: 20,
  },
})

*Defined in DialogContent/styles.ts:3*

#### Type declaration

 container: `object`

 padding: `number`

___


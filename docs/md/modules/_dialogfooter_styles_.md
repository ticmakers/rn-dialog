[@ticmakers-react-native/dialog](../README.md) > ["DialogFooter/styles"](../modules/_dialogfooter_styles_.md)

# External module: "DialogFooter/styles"

## Index

### Variables

* [styles](_dialogfooter_styles_.md#styles)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
  },

  border: {
    borderColor: '#CCD0D5',
    borderTopWidth: 1 / PixelRatio.get(),
  },

  actionsVertical: {
    flexDirection: 'column',
    height: 200,
  },

  actionsHorizontal: {
    flexDirection: 'row',
  },
})

*Defined in DialogFooter/styles.ts:3*

#### Type declaration

 container: `object`

 actionsHorizontal: `object`

 flexDirection: "row"

 actionsVertical: `object`

 flexDirection: "column"

 height: `number`

 border: `object`

 borderColor: `string`

 borderTopWidth: `number`

___


[@ticmakers-react-native/dialog](../README.md) > ["DialogTitle/index.d"](../modules/_dialogtitle_index_d_.md) > [IDialogTitleProps](../interfaces/_dialogtitle_index_d_.idialogtitleprops.md)

# Interface: IDialogTitleProps

Interface to define the props of the DialogTitle component

*__interface__*: IDialogTitleProps

*__extends__*: {IDialogTitleState}

## Hierarchy

 [IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md)

**↳ IDialogTitleProps**

## Index

### Properties

* [align](_dialogtitle_index_d_.idialogtitleprops.md#align)
* [containerStyle](_dialogtitle_index_d_.idialogtitleprops.md#containerstyle)
* [hasTitleBar](_dialogtitle_index_d_.idialogtitleprops.md#hastitlebar)
* [options](_dialogtitle_index_d_.idialogtitleprops.md#options)
* [style](_dialogtitle_index_d_.idialogtitleprops.md#style)
* [title](_dialogtitle_index_d_.idialogtitleprops.md#title)

---

## Properties

<a id="align"></a>

### `<Optional>` align

**● align**: *[TypeDialogTitleAlign](../modules/_dialogtitle_index_d_.md#typedialogtitlealign)*

*Inherited from [IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md).[align](_dialogtitle_index_d_.idialogtitlestate.md#align)*

*Defined in DialogTitle/index.d.ts:21*

Set a align value to the button (left\|center\|right)

*__type__*: {TypeDialogTitleAlign}

*__memberof__*: IDialogTitleState

*__default__*: left

___
<a id="containerstyle"></a>

### `<Optional>` containerStyle

**● containerStyle**: *`TypeStyle`*

*Inherited from [IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md).[containerStyle](_dialogtitle_index_d_.idialogtitlestate.md#containerstyle)*

*Defined in DialogTitle/index.d.ts:28*

Apply a custom style to the container of the dialog title

*__type__*: {TypeStyle}

*__memberof__*: IDialogTitleState

___
<a id="hastitlebar"></a>

### `<Optional>` hasTitleBar

**● hasTitleBar**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md).[hasTitleBar](_dialogtitle_index_d_.idialogtitlestate.md#hastitlebar)*

*Defined in DialogTitle/index.d.ts:36*

Set true to define a title bar

*__type__*: {boolean}

*__memberof__*: IDialogTitleState

*__default__*: true

___
<a id="options"></a>

### `<Optional>` options

**● options**: *[IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md)*

*Defined in DialogTitle/index.d.ts:64*

Prop for group all the props of the DialogTitle component

*__type__*: {IDialogTitleState}

*__memberof__*: IDialogTitleProps

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Inherited from [IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md).[style](_dialogtitle_index_d_.idialogtitlestate.md#style)*

*Defined in DialogTitle/index.d.ts:43*

Apply a custom style to the dialog title

*__type__*: {TypeStyle}

*__memberof__*: IDialogTitleState

___
<a id="title"></a>

### `<Optional>` title

**● title**: *`undefined` \| `string`*

*Inherited from [IDialogTitleState](_dialogtitle_index_d_.idialogtitlestate.md).[title](_dialogtitle_index_d_.idialogtitlestate.md#title)*

*Defined in DialogTitle/index.d.ts:50*

A string to define the title of the dialog

*__type__*: {string}

*__memberof__*: IDialogTitleState

___


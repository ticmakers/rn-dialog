[@ticmakers-react-native/dialog](../README.md) > ["DialogComponent/index.d"](../modules/_dialogcomponent_index_d_.md) > [IDialogProps](../interfaces/_dialogcomponent_index_d_.idialogprops.md)

# Interface: IDialogProps

Interface to define the props of the Dialog component

*__interface__*: IDialogProps

*__extends__*: {IDialogState}

## Hierarchy

 [IDialogState](_dialogcomponent_index_d_.idialogstate.md)

**↳ IDialogProps**

## Index

### Properties

* [animation](_dialogcomponent_index_d_.idialogprops.md#animation)
* [animationDuration](_dialogcomponent_index_d_.idialogprops.md#animationduration)
* [children](_dialogcomponent_index_d_.idialogprops.md#children)
* [containerStyle](_dialogcomponent_index_d_.idialogprops.md#containerstyle)
* [dismissOnHardwareBackPress](_dialogcomponent_index_d_.idialogprops.md#dismissonhardwarebackpress)
* [dismissOnTouchOutside](_dialogcomponent_index_d_.idialogprops.md#dismissontouchoutside)
* [footer](_dialogcomponent_index_d_.idialogprops.md#footer)
* [hasOverlay](_dialogcomponent_index_d_.idialogprops.md#hasoverlay)
* [height](_dialogcomponent_index_d_.idialogprops.md#height)
* [onDismiss](_dialogcomponent_index_d_.idialogprops.md#ondismiss)
* [onShow](_dialogcomponent_index_d_.idialogprops.md#onshow)
* [options](_dialogcomponent_index_d_.idialogprops.md#options)
* [overlayBackgroundColor](_dialogcomponent_index_d_.idialogprops.md#overlaybackgroundcolor)
* [overlayOpacity](_dialogcomponent_index_d_.idialogprops.md#overlayopacity)
* [overlayPointerEvents](_dialogcomponent_index_d_.idialogprops.md#overlaypointerevents)
* [rounded](_dialogcomponent_index_d_.idialogprops.md#rounded)
* [slideFrom](_dialogcomponent_index_d_.idialogprops.md#slidefrom)
* [style](_dialogcomponent_index_d_.idialogprops.md#style)
* [title](_dialogcomponent_index_d_.idialogprops.md#title)
* [titleStyle](_dialogcomponent_index_d_.idialogprops.md#titlestyle)
* [useNativeDriver](_dialogcomponent_index_d_.idialogprops.md#usenativedriver)
* [visible](_dialogcomponent_index_d_.idialogprops.md#visible)
* [width](_dialogcomponent_index_d_.idialogprops.md#width)

---

## Properties

<a id="animation"></a>

### `<Optional>` animation

**● animation**: *[TypeDialogAnimation](../modules/_dialogcomponent_index_d_.md#typedialoganimation)*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[animation](_dialogcomponent_index_d_.idialogstate.md#animation)*

*Defined in DialogComponent/index.d.ts:27*

Define the animation to show the Dialog

*__type__*: {TypeDialogAnimation}

*__memberof__*: IDialogState

*__default__*: fade

___
<a id="animationduration"></a>

### `<Optional>` animationDuration

**● animationDuration**: *`undefined` \| `number`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[animationDuration](_dialogcomponent_index_d_.idialogstate.md#animationduration)*

*Defined in DialogComponent/index.d.ts:34*

A number millisecond to set the time animation

*__type__*: {number}

*__memberof__*: IDialogState

___
<a id="children"></a>

### `<Optional>` children

**● children**: *`TypeComponent` \| `TypeComponent`[]*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[children](_dialogcomponent_index_d_.idialogstate.md#children)*

*Defined in DialogComponent/index.d.ts:41*

Define the content or children for the dialog

*__type__*: {(TypeComponent \| TypeComponent\[\])}

*__memberof__*: IDialogState

___
<a id="containerstyle"></a>

### `<Optional>` containerStyle

**● containerStyle**: *`TypeStyle`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[containerStyle](_dialogcomponent_index_d_.idialogstate.md#containerstyle)*

*Defined in DialogComponent/index.d.ts:48*

Apply a custom style to the content of the dialog

*__type__*: {TypeStyle}

*__memberof__*: IDialogState

___
<a id="dismissonhardwarebackpress"></a>

### `<Optional>` dismissOnHardwareBackPress

**● dismissOnHardwareBackPress**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[dismissOnHardwareBackPress](_dialogcomponent_index_d_.idialogstate.md#dismissonhardwarebackpress)*

*Defined in DialogComponent/index.d.ts:56*

Set true to enable the dismiss when pressed the button back

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="dismissontouchoutside"></a>

### `<Optional>` dismissOnTouchOutside

**● dismissOnTouchOutside**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[dismissOnTouchOutside](_dialogcomponent_index_d_.idialogstate.md#dismissontouchoutside)*

*Defined in DialogComponent/index.d.ts:64*

Set true to enable the dismiss when pressed outside of dialog

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="footer"></a>

### `<Optional>` footer

**● footer**: *`TypeComponent` \| `TypeComponent`[]*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[footer](_dialogcomponent_index_d_.idialogstate.md#footer)*

*Defined in DialogComponent/index.d.ts:71*

List of the buttons for the actions

*__type__*: {(TypeComponent \| TypeComponent\[\])}

*__memberof__*: IDialogState

___
<a id="hasoverlay"></a>

### `<Optional>` hasOverlay

**● hasOverlay**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[hasOverlay](_dialogcomponent_index_d_.idialogstate.md#hasoverlay)*

*Defined in DialogComponent/index.d.ts:79*

Set true if the dialog has overlay

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="height"></a>

### `<Optional>` height

**● height**: *`number` \| `null`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[height](_dialogcomponent_index_d_.idialogstate.md#height)*

*Defined in DialogComponent/index.d.ts:86*

A number to define the height of the dialog

*__type__*: {(number \| null)}

*__memberof__*: IDialogState

___
<a id="ondismiss"></a>

### `<Optional>` onDismiss

**● onDismiss**: *`undefined` \| `function`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[onDismiss](_dialogcomponent_index_d_.idialogstate.md#ondismiss)*

*Defined in DialogComponent/index.d.ts:92*

Method that fire when the dialog is dismissed

*__memberof__*: IDialogState

___
<a id="onshow"></a>

### `<Optional>` onShow

**● onShow**: *`undefined` \| `function`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[onShow](_dialogcomponent_index_d_.idialogstate.md#onshow)*

*Defined in DialogComponent/index.d.ts:98*

Method that fire when the dialog is showed

*__memberof__*: IDialogState

___
<a id="options"></a>

### `<Optional>` options

**● options**: *[IDialogState](_dialogcomponent_index_d_.idialogstate.md)*

*Defined in DialogComponent/index.d.ts:191*

Prop for group all the props of the Dialog component

*__type__*: {IDialogState}

*__memberof__*: IDialogProps

___
<a id="overlaybackgroundcolor"></a>

### `<Optional>` overlayBackgroundColor

**● overlayBackgroundColor**: *`undefined` \| `string`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[overlayBackgroundColor](_dialogcomponent_index_d_.idialogstate.md#overlaybackgroundcolor)*

*Defined in DialogComponent/index.d.ts:105*

Set a custom background color for the overlay

*__type__*: {string}

*__memberof__*: IDialogState

___
<a id="overlayopacity"></a>

### `<Optional>` overlayOpacity

**● overlayOpacity**: *`undefined` \| `number`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[overlayOpacity](_dialogcomponent_index_d_.idialogstate.md#overlayopacity)*

*Defined in DialogComponent/index.d.ts:112*

Set a custom opacity for the overlay

*__type__*: {number}

*__memberof__*: IDialogState

___
<a id="overlaypointerevents"></a>

### `<Optional>` overlayPointerEvents

**● overlayPointerEvents**: *"auto" \| "none"*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[overlayPointerEvents](_dialogcomponent_index_d_.idialogstate.md#overlaypointerevents)*

*Defined in DialogComponent/index.d.ts:120*

Define a custom overlay pointer events

*__type__*: {('auto' \| 'none')}

*__memberof__*: IDialogState

*__default__*: auto

___
<a id="rounded"></a>

### `<Optional>` rounded

**● rounded**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[rounded](_dialogcomponent_index_d_.idialogstate.md#rounded)*

*Defined in DialogComponent/index.d.ts:127*

Set true if the dialog is rounded style

*__type__*: {boolean}

*__memberof__*: IDialogState

___
<a id="slidefrom"></a>

### `<Optional>` slideFrom

**● slideFrom**: *[TypeDialogSlideFrom](../modules/_dialogcomponent_index_d_.md#typedialogslidefrom)*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[slideFrom](_dialogcomponent_index_d_.idialogstate.md#slidefrom)*

*Defined in DialogComponent/index.d.ts:134*

Define a custom animation for the animation slide

*__type__*: {TypeDialogSlideFrom}

*__memberof__*: IDialogState

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[style](_dialogcomponent_index_d_.idialogstate.md#style)*

*Defined in DialogComponent/index.d.ts:141*

Apply a custom style to the dialog

*__type__*: {TypeStyle}

*__memberof__*: IDialogState

___
<a id="title"></a>

### `<Optional>` title

**● title**: *`string` \| `TypeComponent`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[title](_dialogcomponent_index_d_.idialogstate.md#title)*

*Defined in DialogComponent/index.d.ts:148*

A string to define the title of the dialog

*__type__*: {(string \| TypeComponent)}

*__memberof__*: IDialogState

___
<a id="titlestyle"></a>

### `<Optional>` titleStyle

**● titleStyle**: *`TypeStyle`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[titleStyle](_dialogcomponent_index_d_.idialogstate.md#titlestyle)*

*Defined in DialogComponent/index.d.ts:155*

Apply a custom style to the title dialog

*__type__*: {TypeStyle}

*__memberof__*: IDialogState

___
<a id="usenativedriver"></a>

### `<Optional>` useNativeDriver

**● useNativeDriver**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[useNativeDriver](_dialogcomponent_index_d_.idialogstate.md#usenativedriver)*

*Defined in DialogComponent/index.d.ts:163*

Set true to use a native driver

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="visible"></a>

### `<Optional>` visible

**● visible**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[visible](_dialogcomponent_index_d_.idialogstate.md#visible)*

*Defined in DialogComponent/index.d.ts:170*

Set true to make visible the dialog

*__type__*: {boolean}

*__memberof__*: IDialogState

___
<a id="width"></a>

### `<Optional>` width

**● width**: *`number` \| `null`*

*Inherited from [IDialogState](_dialogcomponent_index_d_.idialogstate.md).[width](_dialogcomponent_index_d_.idialogstate.md#width)*

*Defined in DialogComponent/index.d.ts:177*

A number to define the height of the dialog

*__type__*: {(number \| null)}

*__memberof__*: IDialogState

___


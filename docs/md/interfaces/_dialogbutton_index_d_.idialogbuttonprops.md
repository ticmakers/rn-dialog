[@ticmakers-react-native/dialog](../README.md) > ["DialogButton/index.d"](../modules/_dialogbutton_index_d_.md) > [IDialogButtonProps](../interfaces/_dialogbutton_index_d_.idialogbuttonprops.md)

# Interface: IDialogButtonProps

Interface to define the props of the DialogButton component

*__interface__*: IDialogButtonProps

*__extends__*: {IDialogButtonState}

## Hierarchy

 [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md)

**↳ IDialogButtonProps**

## Index

### Properties

* [activeOpacity](_dialogbutton_index_d_.idialogbuttonprops.md#activeopacity)
* [align](_dialogbutton_index_d_.idialogbuttonprops.md#align)
* [bordered](_dialogbutton_index_d_.idialogbuttonprops.md#bordered)
* [disabled](_dialogbutton_index_d_.idialogbuttonprops.md#disabled)
* [onPress](_dialogbutton_index_d_.idialogbuttonprops.md#onpress)
* [options](_dialogbutton_index_d_.idialogbuttonprops.md#options)
* [style](_dialogbutton_index_d_.idialogbuttonprops.md#style)
* [title](_dialogbutton_index_d_.idialogbuttonprops.md#title)
* [titleStyle](_dialogbutton_index_d_.idialogbuttonprops.md#titlestyle)

---

## Properties

<a id="activeopacity"></a>

### `<Optional>` activeOpacity

**● activeOpacity**: *`undefined` \| `number`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[activeOpacity](_dialogbutton_index_d_.idialogbuttonstate.md#activeopacity)*

*Defined in DialogButton/index.d.ts:20*

A number to set the active opacity

*__type__*: {number}

*__memberof__*: IDialogButtonState

___
<a id="align"></a>

### `<Optional>` align

**● align**: *[TypeDialogButtonAlign](../modules/_dialogbutton_index_d_.md#typedialogbuttonalign)*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[align](_dialogbutton_index_d_.idialogbuttonstate.md#align)*

*Defined in DialogButton/index.d.ts:28*

Set a align value to the button (left\|center\|right)

*__type__*: {TypeDialogButtonAlign}

*__memberof__*: IDialogButtonState

*__default__*: left

___
<a id="bordered"></a>

### `<Optional>` bordered

**● bordered**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[bordered](_dialogbutton_index_d_.idialogbuttonstate.md#bordered)*

*Defined in DialogButton/index.d.ts:36*

Set true to apply a border to the button

*__type__*: {boolean}

*__memberof__*: IDialogButtonState

*__default__*: false

___
<a id="disabled"></a>

### `<Optional>` disabled

**● disabled**: *`undefined` \| `false` \| `true`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[disabled](_dialogbutton_index_d_.idialogbuttonstate.md#disabled)*

*Defined in DialogButton/index.d.ts:44*

Set true to disable the button

*__type__*: {boolean}

*__memberof__*: IDialogButtonState

*__default__*: false

___
<a id="onpress"></a>

### `<Optional>` onPress

**● onPress**: *`undefined` \| `function`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[onPress](_dialogbutton_index_d_.idialogbuttonstate.md#onpress)*

*Defined in DialogButton/index.d.ts:50*

Method that fire when the button is pressed

*__memberof__*: IDialogButtonState

___
<a id="options"></a>

### `<Optional>` options

**● options**: *[IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md)*

*Defined in DialogButton/index.d.ts:83*

Prop for group all the props of the DialogButton component

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[style](_dialogbutton_index_d_.idialogbuttonstate.md#style)*

*Defined in DialogButton/index.d.ts:57*

Apply a custom style to the button

*__type__*: {TypeStyle}

*__memberof__*: IDialogButtonState

___
<a id="title"></a>

### `<Optional>` title

**● title**: *`undefined` \| `string`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[title](_dialogbutton_index_d_.idialogbuttonstate.md#title)*

*Defined in DialogButton/index.d.ts:64*

A string to define the title of the button

*__type__*: {string}

*__memberof__*: IDialogButtonState

___
<a id="titlestyle"></a>

### `<Optional>` titleStyle

**● titleStyle**: *`TypeStyle`*

*Inherited from [IDialogButtonState](_dialogbutton_index_d_.idialogbuttonstate.md).[titleStyle](_dialogbutton_index_d_.idialogbuttonstate.md#titlestyle)*

*Defined in DialogButton/index.d.ts:71*

Apply a custom style to the title of the button

*__type__*: {TypeStyle}

*__memberof__*: IDialogButtonState

___


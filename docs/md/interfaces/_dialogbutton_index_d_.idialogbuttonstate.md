[@ticmakers-react-native/dialog](../README.md) > ["DialogButton/index.d"](../modules/_dialogbutton_index_d_.md) > [IDialogButtonState](../interfaces/_dialogbutton_index_d_.idialogbuttonstate.md)

# Interface: IDialogButtonState

Interface to define the states of the DialogButton component

*__interface__*: IDialogButtonState

## Hierarchy

**IDialogButtonState**

↳  [IDialogButtonProps](_dialogbutton_index_d_.idialogbuttonprops.md)

## Index

### Properties

* [activeOpacity](_dialogbutton_index_d_.idialogbuttonstate.md#activeopacity)
* [align](_dialogbutton_index_d_.idialogbuttonstate.md#align)
* [bordered](_dialogbutton_index_d_.idialogbuttonstate.md#bordered)
* [disabled](_dialogbutton_index_d_.idialogbuttonstate.md#disabled)
* [onPress](_dialogbutton_index_d_.idialogbuttonstate.md#onpress)
* [style](_dialogbutton_index_d_.idialogbuttonstate.md#style)
* [title](_dialogbutton_index_d_.idialogbuttonstate.md#title)
* [titleStyle](_dialogbutton_index_d_.idialogbuttonstate.md#titlestyle)

---

## Properties

<a id="activeopacity"></a>

### `<Optional>` activeOpacity

**● activeOpacity**: *`undefined` \| `number`*

*Defined in DialogButton/index.d.ts:20*

A number to set the active opacity

*__type__*: {number}

*__memberof__*: IDialogButtonState

___
<a id="align"></a>

### `<Optional>` align

**● align**: *[TypeDialogButtonAlign](../modules/_dialogbutton_index_d_.md#typedialogbuttonalign)*

*Defined in DialogButton/index.d.ts:28*

Set a align value to the button (left\|center\|right)

*__type__*: {TypeDialogButtonAlign}

*__memberof__*: IDialogButtonState

*__default__*: left

___
<a id="bordered"></a>

### `<Optional>` bordered

**● bordered**: *`undefined` \| `false` \| `true`*

*Defined in DialogButton/index.d.ts:36*

Set true to apply a border to the button

*__type__*: {boolean}

*__memberof__*: IDialogButtonState

*__default__*: false

___
<a id="disabled"></a>

### `<Optional>` disabled

**● disabled**: *`undefined` \| `false` \| `true`*

*Defined in DialogButton/index.d.ts:44*

Set true to disable the button

*__type__*: {boolean}

*__memberof__*: IDialogButtonState

*__default__*: false

___
<a id="onpress"></a>

### `<Optional>` onPress

**● onPress**: *`undefined` \| `function`*

*Defined in DialogButton/index.d.ts:50*

Method that fire when the button is pressed

*__memberof__*: IDialogButtonState

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Defined in DialogButton/index.d.ts:57*

Apply a custom style to the button

*__type__*: {TypeStyle}

*__memberof__*: IDialogButtonState

___
<a id="title"></a>

### `<Optional>` title

**● title**: *`undefined` \| `string`*

*Defined in DialogButton/index.d.ts:64*

A string to define the title of the button

*__type__*: {string}

*__memberof__*: IDialogButtonState

___
<a id="titlestyle"></a>

### `<Optional>` titleStyle

**● titleStyle**: *`TypeStyle`*

*Defined in DialogButton/index.d.ts:71*

Apply a custom style to the title of the button

*__type__*: {TypeStyle}

*__memberof__*: IDialogButtonState

___


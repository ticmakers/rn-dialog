[@ticmakers-react-native/dialog](../README.md) > ["DialogComponent/index.d"](../modules/_dialogcomponent_index_d_.md) > [IDialogState](../interfaces/_dialogcomponent_index_d_.idialogstate.md)

# Interface: IDialogState

Interface to define the states of the Dialog component

*__interface__*: IDialogState

## Hierarchy

**IDialogState**

↳  [IDialogProps](_dialogcomponent_index_d_.idialogprops.md)

## Index

### Properties

* [animation](_dialogcomponent_index_d_.idialogstate.md#animation)
* [animationDuration](_dialogcomponent_index_d_.idialogstate.md#animationduration)
* [children](_dialogcomponent_index_d_.idialogstate.md#children)
* [containerStyle](_dialogcomponent_index_d_.idialogstate.md#containerstyle)
* [dismissOnHardwareBackPress](_dialogcomponent_index_d_.idialogstate.md#dismissonhardwarebackpress)
* [dismissOnTouchOutside](_dialogcomponent_index_d_.idialogstate.md#dismissontouchoutside)
* [footer](_dialogcomponent_index_d_.idialogstate.md#footer)
* [hasOverlay](_dialogcomponent_index_d_.idialogstate.md#hasoverlay)
* [height](_dialogcomponent_index_d_.idialogstate.md#height)
* [onDismiss](_dialogcomponent_index_d_.idialogstate.md#ondismiss)
* [onShow](_dialogcomponent_index_d_.idialogstate.md#onshow)
* [overlayBackgroundColor](_dialogcomponent_index_d_.idialogstate.md#overlaybackgroundcolor)
* [overlayOpacity](_dialogcomponent_index_d_.idialogstate.md#overlayopacity)
* [overlayPointerEvents](_dialogcomponent_index_d_.idialogstate.md#overlaypointerevents)
* [rounded](_dialogcomponent_index_d_.idialogstate.md#rounded)
* [slideFrom](_dialogcomponent_index_d_.idialogstate.md#slidefrom)
* [style](_dialogcomponent_index_d_.idialogstate.md#style)
* [title](_dialogcomponent_index_d_.idialogstate.md#title)
* [titleStyle](_dialogcomponent_index_d_.idialogstate.md#titlestyle)
* [useNativeDriver](_dialogcomponent_index_d_.idialogstate.md#usenativedriver)
* [visible](_dialogcomponent_index_d_.idialogstate.md#visible)
* [width](_dialogcomponent_index_d_.idialogstate.md#width)

---

## Properties

<a id="animation"></a>

### `<Optional>` animation

**● animation**: *[TypeDialogAnimation](../modules/_dialogcomponent_index_d_.md#typedialoganimation)*

*Defined in DialogComponent/index.d.ts:27*

Define the animation to show the Dialog

*__type__*: {TypeDialogAnimation}

*__memberof__*: IDialogState

*__default__*: fade

___
<a id="animationduration"></a>

### `<Optional>` animationDuration

**● animationDuration**: *`undefined` \| `number`*

*Defined in DialogComponent/index.d.ts:34*

A number millisecond to set the time animation

*__type__*: {number}

*__memberof__*: IDialogState

___
<a id="children"></a>

### `<Optional>` children

**● children**: *`TypeComponent` \| `TypeComponent`[]*

*Defined in DialogComponent/index.d.ts:41*

Define the content or children for the dialog

*__type__*: {(TypeComponent \| TypeComponent\[\])}

*__memberof__*: IDialogState

___
<a id="containerstyle"></a>

### `<Optional>` containerStyle

**● containerStyle**: *`TypeStyle`*

*Defined in DialogComponent/index.d.ts:48*

Apply a custom style to the content of the dialog

*__type__*: {TypeStyle}

*__memberof__*: IDialogState

___
<a id="dismissonhardwarebackpress"></a>

### `<Optional>` dismissOnHardwareBackPress

**● dismissOnHardwareBackPress**: *`undefined` \| `false` \| `true`*

*Defined in DialogComponent/index.d.ts:56*

Set true to enable the dismiss when pressed the button back

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="dismissontouchoutside"></a>

### `<Optional>` dismissOnTouchOutside

**● dismissOnTouchOutside**: *`undefined` \| `false` \| `true`*

*Defined in DialogComponent/index.d.ts:64*

Set true to enable the dismiss when pressed outside of dialog

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="footer"></a>

### `<Optional>` footer

**● footer**: *`TypeComponent` \| `TypeComponent`[]*

*Defined in DialogComponent/index.d.ts:71*

List of the buttons for the actions

*__type__*: {(TypeComponent \| TypeComponent\[\])}

*__memberof__*: IDialogState

___
<a id="hasoverlay"></a>

### `<Optional>` hasOverlay

**● hasOverlay**: *`undefined` \| `false` \| `true`*

*Defined in DialogComponent/index.d.ts:79*

Set true if the dialog has overlay

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="height"></a>

### `<Optional>` height

**● height**: *`number` \| `null`*

*Defined in DialogComponent/index.d.ts:86*

A number to define the height of the dialog

*__type__*: {(number \| null)}

*__memberof__*: IDialogState

___
<a id="ondismiss"></a>

### `<Optional>` onDismiss

**● onDismiss**: *`undefined` \| `function`*

*Defined in DialogComponent/index.d.ts:92*

Method that fire when the dialog is dismissed

*__memberof__*: IDialogState

___
<a id="onshow"></a>

### `<Optional>` onShow

**● onShow**: *`undefined` \| `function`*

*Defined in DialogComponent/index.d.ts:98*

Method that fire when the dialog is showed

*__memberof__*: IDialogState

___
<a id="overlaybackgroundcolor"></a>

### `<Optional>` overlayBackgroundColor

**● overlayBackgroundColor**: *`undefined` \| `string`*

*Defined in DialogComponent/index.d.ts:105*

Set a custom background color for the overlay

*__type__*: {string}

*__memberof__*: IDialogState

___
<a id="overlayopacity"></a>

### `<Optional>` overlayOpacity

**● overlayOpacity**: *`undefined` \| `number`*

*Defined in DialogComponent/index.d.ts:112*

Set a custom opacity for the overlay

*__type__*: {number}

*__memberof__*: IDialogState

___
<a id="overlaypointerevents"></a>

### `<Optional>` overlayPointerEvents

**● overlayPointerEvents**: *"auto" \| "none"*

*Defined in DialogComponent/index.d.ts:120*

Define a custom overlay pointer events

*__type__*: {('auto' \| 'none')}

*__memberof__*: IDialogState

*__default__*: auto

___
<a id="rounded"></a>

### `<Optional>` rounded

**● rounded**: *`undefined` \| `false` \| `true`*

*Defined in DialogComponent/index.d.ts:127*

Set true if the dialog is rounded style

*__type__*: {boolean}

*__memberof__*: IDialogState

___
<a id="slidefrom"></a>

### `<Optional>` slideFrom

**● slideFrom**: *[TypeDialogSlideFrom](../modules/_dialogcomponent_index_d_.md#typedialogslidefrom)*

*Defined in DialogComponent/index.d.ts:134*

Define a custom animation for the animation slide

*__type__*: {TypeDialogSlideFrom}

*__memberof__*: IDialogState

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Defined in DialogComponent/index.d.ts:141*

Apply a custom style to the dialog

*__type__*: {TypeStyle}

*__memberof__*: IDialogState

___
<a id="title"></a>

### `<Optional>` title

**● title**: *`string` \| `TypeComponent`*

*Defined in DialogComponent/index.d.ts:148*

A string to define the title of the dialog

*__type__*: {(string \| TypeComponent)}

*__memberof__*: IDialogState

___
<a id="titlestyle"></a>

### `<Optional>` titleStyle

**● titleStyle**: *`TypeStyle`*

*Defined in DialogComponent/index.d.ts:155*

Apply a custom style to the title dialog

*__type__*: {TypeStyle}

*__memberof__*: IDialogState

___
<a id="usenativedriver"></a>

### `<Optional>` useNativeDriver

**● useNativeDriver**: *`undefined` \| `false` \| `true`*

*Defined in DialogComponent/index.d.ts:163*

Set true to use a native driver

*__type__*: {boolean}

*__memberof__*: IDialogState

*__default__*: true

___
<a id="visible"></a>

### `<Optional>` visible

**● visible**: *`undefined` \| `false` \| `true`*

*Defined in DialogComponent/index.d.ts:170*

Set true to make visible the dialog

*__type__*: {boolean}

*__memberof__*: IDialogState

___
<a id="width"></a>

### `<Optional>` width

**● width**: *`number` \| `null`*

*Defined in DialogComponent/index.d.ts:177*

A number to define the height of the dialog

*__type__*: {(number \| null)}

*__memberof__*: IDialogState

___


[@ticmakers-react-native/dialog](../README.md) > ["DialogTitle/index.d"](../modules/_dialogtitle_index_d_.md) > [IDialogTitleState](../interfaces/_dialogtitle_index_d_.idialogtitlestate.md)

# Interface: IDialogTitleState

Interface to define the states of the DialogTitle component

*__interface__*: IDialogTitleState

## Hierarchy

**IDialogTitleState**

↳  [IDialogTitleProps](_dialogtitle_index_d_.idialogtitleprops.md)

## Index

### Properties

* [align](_dialogtitle_index_d_.idialogtitlestate.md#align)
* [containerStyle](_dialogtitle_index_d_.idialogtitlestate.md#containerstyle)
* [hasTitleBar](_dialogtitle_index_d_.idialogtitlestate.md#hastitlebar)
* [style](_dialogtitle_index_d_.idialogtitlestate.md#style)
* [title](_dialogtitle_index_d_.idialogtitlestate.md#title)

---

## Properties

<a id="align"></a>

### `<Optional>` align

**● align**: *[TypeDialogTitleAlign](../modules/_dialogtitle_index_d_.md#typedialogtitlealign)*

*Defined in DialogTitle/index.d.ts:21*

Set a align value to the button (left\|center\|right)

*__type__*: {TypeDialogTitleAlign}

*__memberof__*: IDialogTitleState

*__default__*: left

___
<a id="containerstyle"></a>

### `<Optional>` containerStyle

**● containerStyle**: *`TypeStyle`*

*Defined in DialogTitle/index.d.ts:28*

Apply a custom style to the container of the dialog title

*__type__*: {TypeStyle}

*__memberof__*: IDialogTitleState

___
<a id="hastitlebar"></a>

### `<Optional>` hasTitleBar

**● hasTitleBar**: *`undefined` \| `false` \| `true`*

*Defined in DialogTitle/index.d.ts:36*

Set true to define a title bar

*__type__*: {boolean}

*__memberof__*: IDialogTitleState

*__default__*: true

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Defined in DialogTitle/index.d.ts:43*

Apply a custom style to the dialog title

*__type__*: {TypeStyle}

*__memberof__*: IDialogTitleState

___
<a id="title"></a>

### `<Optional>` title

**● title**: *`undefined` \| `string`*

*Defined in DialogTitle/index.d.ts:50*

A string to define the title of the dialog

*__type__*: {string}

*__memberof__*: IDialogTitleState

___


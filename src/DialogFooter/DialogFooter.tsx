import * as React from 'react'
import { StyleSheet, View } from 'react-native'

import { TypeComponent } from '@ticmakers-react-native/core'
import { IDialogFooterProps, IDialogFooterState } from './../../index'
import styles from './styles'

/**
 * Class to define the component DialogFooter used in Dialog
 * @class DialogFooter
 * @extends {React.Component<IDialogFooterProps, IDialogFooterState>}
 */
export default class DialogFooter extends React.Component<IDialogFooterProps, IDialogFooterState> {
  /**
   * Creates an instance of DialogFooter.
   * @param {IDialogFooterProps} props     Props of the component
   * @memberof DialogFooter
   */
  constructor(props: IDialogFooterProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogFooter
   */
  public render(): TypeComponent {
    const { bordered, style } = this._processProps()
    const { children } = this.props

    const containerStyle = (children as any).length > 2 ? styles.actionsVertical : styles.actionsHorizontal
    let content = children

    if ((children as any).length === 2) {
      content = React.Children.map(children, (child: any, index: number) => React.cloneElement(child, { bordered: (1 % index === 0 && bordered) }))
    }

    const props: IDialogFooterProps = {
      style: StyleSheet.flatten([styles.container, containerStyle, bordered && styles.border, style]),
    }

    return <View { ...props }>{ content }</View>
  }

  /**
   * Method that process the props
   * @private
   * @returns {IDialogFooterState}
   * @memberof DialogFooter
   */
  private _processProps(): IDialogFooterState {
    const { bordered, options, style } = this.props

    const props: IDialogFooterState = {
      bordered: (options && options.bordered) || (bordered || true),
      style: (options && options.style) || (style || undefined),
    }

    return props
  }
}

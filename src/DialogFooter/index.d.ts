import * as React from 'react'
import { ViewProps } from 'react-native'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'

/**
 * Interface to define the states of the DialogFooter component
 * @interface IDialogFooterState
 */
export interface IDialogFooterState extends ViewProps {
  /**
   * Set true to set a style bordered to the footer dialog
   * @type {boolean}
   * @memberof IDialogFooterState
   * @default true
   */
  bordered?: boolean

  /**
   * Apply a custom style to the footer dialog
   * @type {TypeStyle}
   * @memberof IDialogFooterState
   */
  style?: TypeStyle
}

/**
 * Interface to define the props of the DialogFooter component
 * @interface IDialogFooterProps
 * @extends {IDialogFooterState}
 */
export interface IDialogFooterProps extends IDialogFooterState {
  /**
   */
  /**
   * Prop for group all the props of the DialogFooter component
   * @type {IDialogFooterState}
   * @memberof IDialogFooterProps
   */
  options?: IDialogFooterState
}

/**
 * Class to define the component DialogFooter used in Dialog
 * @class DialogFooter
 * @extends {React.Component<IDialogFooterProps, IDialogFooterState>}
 */
declare class DialogFooter extends React.Component<IDialogFooterProps, IDialogFooterState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogFooter
   */
  public render(): TypeComponent

  /**
   * Method that process the props
   * @private
   * @returns {IDialogFooterState}
   * @memberof DialogFooter
   */
  private _processProps(): IDialogFooterState
}

export default DialogFooter

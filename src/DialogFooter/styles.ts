import { StyleSheet, PixelRatio } from 'react-native'

const styles = StyleSheet.create({
  container: {
  },

  border: {
    borderColor: '#CCD0D5',
    borderTopWidth: 1 / PixelRatio.get(),
  },

  actionsVertical: {
    flexDirection: 'column',
    height: 200,
  },

  actionsHorizontal: {
    flexDirection: 'row',
  },
})

export default styles

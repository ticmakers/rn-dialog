import * as React from 'react'
import { ViewProps } from 'react-native'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'

/**
 * Interface to define the states of the DialogContent component
 * @interface IDialogContentState
 */
export interface IDialogContentState extends ViewProps {
  /**
   * Apply a custom style to the dialog content
   * @type {TypeStyle}
   * @memberof IDialogContentState
   */
  style?: TypeStyle
}

/**
 * Interface to define the props of the DialogContent component
 * @interface IDialogContentProps
 * @extends {IDialogContentState}
 */
export interface IDialogContentProps extends IDialogContentState {
  /**
   * Prop for group all the props of the DialogContent component
   * @type {IDialogContentState}
   * @memberof IDialogContentProps
   */
  options?: IDialogContentState
}

/**
 * Class to define the component DialogContent used in Dialog
 * @class DialogContent
 * @extends {React.Component<IDialogContentProps, IDialogContentState>}
 */
declare class DialogContent extends React.Component<IDialogContentProps, IDialogContentState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogContent
   */
  public render(): TypeComponent

  /**
   * Method that process the props
   * @private
   * @returns {IDialogContentState}
   * @memberof DialogContent
   */
  private _processProps(): IDialogContentState
}

export default DialogContent

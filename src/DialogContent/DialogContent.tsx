import * as React from 'react'
import { StyleSheet, View } from 'react-native'

import { TypeComponent } from '@ticmakers-react-native/core'
import { IDialogContentProps, IDialogContentState } from './../../index'
import styles from './styles'

/**
 * Class to define the component DialogContent used in Dialog
 * @class DialogContent
 * @extends {React.Component<IDialogContentProps, IDialogContentState>}
 */
export default class DialogContent extends React.Component<IDialogContentProps, IDialogContentState> {
  /**
   * Creates an instance of DialogContent.
   * @param {IDialogContentProps} props     Props of the component
   * @memberof DialogContent
   */
  constructor(props: IDialogContentProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogContent
   */
  public render(): TypeComponent {
    const { children } = this.props
    const { style } = this._processProps()

    const props: IDialogContentProps = {
      style: StyleSheet.flatten([styles.container, style]),
    }

    return <View { ...props }>{ children }</View>
  }

  /**
   * Method that process the props
   * @private
   * @returns {IDialogContentState}
   * @memberof DialogContent
   */
  private _processProps(): IDialogContentState {
    const { options, style } = this.props

    const props: IDialogContentState = {
      style: (options && options.style) || (style || undefined),
    }

    return props
  }
}

import * as React from 'react'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'

/**
 * Type to define the align position for the DialogButton component
 */
export type TypeDialogButtonAlign = 'left' | 'center' | 'right'

/**
 * Interface to define the states of the DialogButton component
 * @interface IDialogButtonState
 */
export interface IDialogButtonState {
  /**
   * A number to set the active opacity
   * @type {number}
   * @memberof IDialogButtonState
   */
  activeOpacity?: number

  /**
   * Set a align value to the button (left|center|right)
   * @type {TypeDialogButtonAlign}
   * @memberof IDialogButtonState
   * @default left
   */
  align?: TypeDialogButtonAlign

  /**
   * Set true to apply a border to the button
   * @type {boolean}
   * @memberof IDialogButtonState
   * @default false
   */
  bordered?: boolean

  /**
   * Set true to disable the button
   * @type {boolean}
   * @memberof IDialogButtonState
   * @default false
   */
  disabled?: boolean

  /**
   * Method that fire when the button is pressed
   * @memberof IDialogButtonState
   */
  onPress?: () => void

  /**
   * Apply a custom style to the button
   * @type {TypeStyle}
   * @memberof IDialogButtonState
   */
  style?: TypeStyle

  /**
   * A string to define the title of the button
   * @type {string}
   * @memberof IDialogButtonState
   */
  title?: string

  /**
   * Apply a custom style to the title of the button
   * @type {TypeStyle}
   * @memberof IDialogButtonState
   */
  titleStyle?: TypeStyle
}

/**
 * Interface to define the props of the DialogButton component
 * @interface IDialogButtonProps
 * @extends {IDialogButtonState}
 */
export interface IDialogButtonProps extends IDialogButtonState {
  /**
   * Prop for group all the props of the DialogButton component
   */
  options?: IDialogButtonState
}

/**
 * Class to define the component DialogButton used in Dialog
 * @class DialogButton
 * @extends {React.Component<IDialogButtonProps, IDialogButtonState>}
 */
declare class DialogButton extends React.Component<IDialogButtonProps, IDialogButtonState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogButton
   */
  public render(): TypeComponent

  /**
   * Method that fire when the button is pressed
   * @private
   * @returns {void}
   * @memberof DialogButton
   */
  private _onPress(): void

  /**
   * Method that process the props
   * @private
   * @returns {IDialogButtonState}
   * @memberof DialogButton
   */
  private _processProps(): IDialogButtonState
}

export default DialogButton

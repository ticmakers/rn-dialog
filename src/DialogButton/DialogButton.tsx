import * as React from 'react'
import { StyleSheet } from 'react-native'
import { DialogButton as PopupDialogButton, DialogButtonProps } from 'react-native-popup-dialog'

import { TypeComponent } from '@ticmakers-react-native/core'
import { IDialogButtonProps, IDialogButtonState } from './../../index'
import styles from './styles'

/**
 * Class to define the component DialogButton used in Dialog
 * @class DialogButton
 * @extends {React.Component<IDialogButtonProps, IDialogButtonState>}
 */
export default class DialogButton extends React.Component<IDialogButtonProps, IDialogButtonState> {
  /**
   * Creates an instance of DialogButton.
   * @param {IDialogButtonProps} props     Props of the component
   * @memberof DialogButton
   */
  constructor(props: IDialogButtonProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogButton
   */
  public render(): TypeComponent {
    const { activeOpacity, align, bordered, disabled, style, title, titleStyle } = this._processProps()

    const props: DialogButtonProps = {
      activeOpacity,
      align,
      bordered,
      disabled,
      onPress: this._onPress.bind(this),
      style: StyleSheet.flatten([styles.container, style]),
      text: title as string,
      textStyle: StyleSheet.flatten([styles.title, titleStyle]),
    }

    return <PopupDialogButton { ...props } />
  }

  /**
   * Method that fire when the button is pressed
   * @private
   * @returns {void}
   * @memberof DialogButton
   */
  private _onPress(): void {
    const { onPress } = this._processProps()

    if (onPress) {
      return onPress()
    }
  }

  /**
   * Method that process the props
   * @private
   * @returns {IDialogButtonState}
   * @memberof DialogButton
   */
  private _processProps(): IDialogButtonState {
    const { activeOpacity, align, bordered, disabled, onPress, options, style, title, titleStyle } = this.props

    const props: IDialogButtonState = {
      activeOpacity: (options && options.activeOpacity) || (activeOpacity || 0.6),
      align: (options && options.align) || (align || 'center'),
      bordered: (options && options.bordered) || (bordered || false),
      disabled: (options && options.disabled) || (disabled || false),
      onPress: (options && options.onPress) || (onPress || undefined),
      style: (options && options.style) || (style || undefined),
      title: (options && options.title) || (title || undefined),
      titleStyle: (options && options.titleStyle) || (titleStyle || undefined),
    }

    return props
  }
}

import RootSiblings from 'react-native-root-siblings'
import { IDialogProps } from './../DialogComponent/index'

/**
 * Class to define the dialog manager component
 * @class DialogManager
 */
declare class DialogManager {
  /**
   * A number millisecond to define the time to destroy the dialog list components
   * @type {number}
   * @memberof DialogManager
   * @default 150
   */
  public destroyTimeout: number

  /**
   * A list of dialogs to show
   * @type {RootSiblings[]}
   * @memberof DialogManager
   */
  public dialogs: RootSiblings[]

  /**
   * Method to get the current dialog
   * @readonly
   * @type {RootSiblings}
   * @memberof DialogManager
   */
  public currentDialog: RootSiblings

  /**
   * Method to add a new dialog to the list
   * @param {IDialogProps} props      A object of the DialogProps
   * @param {() => void} [callback]   A callback function that fire when the dialog is added
   * @memberof DialogManager
   */
  public add(props: IDialogProps, callback?: () => void): void

  /**
   * Method to destroy a dialog of the list
   * @memberof DialogManager
   */
  public destroy(): void

  /**
   * Method that fire when the a dialog of the list is dismissed
   * @param {() => void} [onDismiss]    A callback function that fire when the dialog is dismissed
   * @memberof DialogManager
   */
  public onDialogDismissed(onDismiss?: () => void): void

  /**
   * Method to update a dialog of the list
   * @param {IDialogProps} props      An object of the DialogProps
   * @param {() => void} [callback]   A callback function that fire when the dialog is updated
   * @memberof DialogManager
   */
  public update(props: IDialogProps, callback?: () => void): void

  /**
   * Method to show the dialog of the list
   * @param {IDialogProps} props      An object of the DialogProps
   * @param {() => any} [callback]    A callback function that fire when the dialog is showed
   * @memberof DialogManager
   */
  public show(props: IDialogProps, callback?: () => any): void

  /**
   * Method to dismiss the dialog of the list
   * @param {() => any} [callback]    A callback function that fire when the dialog is dismissed
   * @memberof DialogManager
   */
  public dismiss(callback?: () => any): void

  /**
   * Method to dismiss all the dialogs of the list
   * @param {() => any} [callback]    A callback function that fire when each dialog is dismissed
   * @memberof DialogManager
   */
  public dismissAll(callback?: () => any): void
}

export default DialogManager

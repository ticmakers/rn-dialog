import * as React from 'react'
import RootSiblings from 'react-native-root-siblings'

import Dialog from './../DialogComponent/Dialog'
import { IDialogProps } from './../../index'

/**
 * Class to define the dialog manager component
 * @class DialogManager
 */
export default class DialogManager {
  /**
   * A number millisecond to define the time to destroy the dialog list components
   * @type {number}
   * @memberof DialogManager
   * @default 150
   */
  public destroyTimeout: number

  /**
   * A list of dialogs to show
   * @type {RootSiblings[]}
   * @memberof DialogManager
   */
  public dialogs: RootSiblings[]

  /**
   * Creates an instance of DialogManager.
   * @memberof DialogManager
   */
  constructor() {
    this.dialogs = []
    this.destroyTimeout = 150
  }

  /**
   * Method to get the current dialog
   * @readonly
   * @type {RootSiblings}
   * @memberof DialogManager
   */
  public get currentDialog(): RootSiblings {
    return this.dialogs[this.dialogs.length - 1]
  }

  /**
   * Method to add a new dialog to the list
   * @param {IDialogProps} props      A object of the DialogProps
   * @param {() => void} [callback]   A callback function that fire when the dialog is added
   * @memberof DialogManager
   */
  public add(props: IDialogProps, callback?: () => void): void {
    const _props = {
      ...props,
      onDismiss: () => this.onDialogDismissed(props.onDismiss),
    }

    const dialog = new RootSiblings(
      <Dialog {..._props} />,
      callback,
    )

    this.dialogs.push(dialog)
  }

  /**
   * Method to destroy a dialog of the list
   * @memberof DialogManager
   */
  public destroy(): void {
    const dialog = this.dialogs.pop()
    setTimeout(() => dialog && dialog.destroy(), this.destroyTimeout)
  }

  /**
   * Method that fire when the a dialog of the list is dismissed
   * @param {() => void} [onDismiss]    A callback function that fire when the dialog is dismissed
   * @memberof DialogManager
   */
  public onDialogDismissed(onDismiss?: () => void): void {
    if (onDismiss) { onDismiss() }
    this.destroy()
  }

  /**
   * Method to update a dialog of the list
   * @param {IDialogProps} props      An object of the DialogProps
   * @param {() => void} [callback]   A callback function that fire when the dialog is updated
   * @memberof DialogManager
   */
  public update(props: IDialogProps, callback?: () => void): void {
    const _props = {
      ...props,
      onDismiss: () => this.onDialogDismissed(props.onDismiss),
    }

    this.currentDialog.update(
      <Dialog {..._props} />,
      callback,
    )
  }

  /**
   * Method to show the dialog of the list
   * @param {IDialogProps} props      An object of the DialogProps
   * @param {() => any} [callback]    A callback function that fire when the dialog is showed
   * @memberof DialogManager
   */
  public show(props: IDialogProps, callback?: () => any): void {
    this.add({ ...props, visible: true }, callback)
  }

  /**
   * Method to dismiss the dialog of the list
   * @param {() => any} [callback]    A callback function that fire when the dialog is dismissed
   * @memberof DialogManager
   */
  public dismiss(callback?: () => any): void {
    this.update({ visible: false }, callback)
  }

  /**
   * Method to dismiss all the dialogs of the list
   * @param {() => any} [callback]    A callback function that fire when each dialog is dismissed
   * @memberof DialogManager
   */
  public dismissAll(callback?: () => any): void {
    this.dialogs.forEach(() => {
      this.dismiss(callback)
    })
  }
}

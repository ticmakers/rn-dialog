import * as React from 'react'
import PopupDialog, { FadeAnimation, ScaleAnimation, SlideAnimation } from 'react-native-popup-dialog'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'

/**
 * Typing to define the type animation for the Dialog component
 */
export type TypeDialogAnimation = 'fade' | 'scale' | 'slide'

/**
 * Typing to define the type animation for the slide animation
 */
export type TypeDialogSlideFrom = 'top' | 'bottom' | 'left' | 'right'

/**
 * Interface to define the states of the Dialog component
 * @interface IDialogState
 */
export interface IDialogState {
  /**
   * Define the animation to show the Dialog
   * @type {TypeDialogAnimation}
   * @memberof IDialogState
   * @default fade
   */
  animation?: TypeDialogAnimation

  /**
   * A number millisecond to set the time animation
   * @type {number}
   * @memberof IDialogState
   */
  animationDuration?: number

  /**
   * Define the content or children for the dialog
   * @type {(TypeComponent | TypeComponent[])}
   * @memberof IDialogState
   */
  children?: TypeComponent | TypeComponent[]

  /**
   * Apply a custom style to the content of the dialog
   * @type {TypeStyle}
   * @memberof IDialogState
   */
  containerStyle?: TypeStyle

  /**
   * Set true to enable the dismiss when pressed the button back
   * @type {boolean}
   * @memberof IDialogState
   * @default true
   */
  dismissOnHardwareBackPress?: boolean

  /**
   * Set true to enable the dismiss when pressed outside of dialog
   * @type {boolean}
   * @memberof IDialogState
   * @default true
   */
  dismissOnTouchOutside?: boolean

  /**
   * List of the buttons for the actions
   * @type {(TypeComponent | TypeComponent[])}
   * @memberof IDialogState
   */
  footer?: TypeComponent | TypeComponent[]

  /**
   * Set true if the dialog has overlay
   * @type {boolean}
   * @memberof IDialogState
   * @default true
   */
  hasOverlay?: boolean

  /**
   * A number to define the height of the dialog
   * @type {(number | null)}
   * @memberof IDialogState
   */
  height?: number | null

  /**
   * Method that fire when the dialog is dismissed
   * @memberof IDialogState
   */
  onDismiss?: () => any

  /**
   * Method that fire when the dialog is showed
   * @memberof IDialogState
   */
  onShow?: () => any

  /**
   * Set a custom background color for the overlay
   * @type {string}
   * @memberof IDialogState
   */
  overlayBackgroundColor?: string

  /**
   * Set a custom opacity for the overlay
   * @type {number}
   * @memberof IDialogState
   */
  overlayOpacity?: number

  /**
   * Define a custom overlay pointer events
   * @type {('auto' | 'none')}
   * @memberof IDialogState
   * @default auto
   */
  overlayPointerEvents?: 'auto' | 'none'

  /**
   * Set true if the dialog is rounded style
   * @type {boolean}
   * @memberof IDialogState
   */
  rounded?: boolean

  /**
   * Define a custom animation for the animation slide
   * @type {TypeDialogSlideFrom}
   * @memberof IDialogState
   */
  slideFrom?: TypeDialogSlideFrom

  /**
   * Apply a custom style to the dialog
   * @type {TypeStyle}
   * @memberof IDialogState
   */
  style?: TypeStyle

  /**
   * A string to define the title of the dialog
   * @type {(string | TypeComponent)}
   * @memberof IDialogState
   */
  title?: string | TypeComponent

  /**
   * Apply a custom style to the title dialog
   * @type {TypeStyle}
   * @memberof IDialogState
   */
  titleStyle?: TypeStyle

  /**
   * Set true to use a native driver
   * @type {boolean}
   * @memberof IDialogState
   * @default true
   */
  useNativeDriver?: boolean

  /**
   * Set true to make visible the dialog
   * @type {boolean}
   * @memberof IDialogState
   */
  visible?: boolean

  /**
   * A number to define the height of the dialog
   * @type {(number | null)}
   * @memberof IDialogState
   */
  width?: number | null
}

/**
 * Interface to define the props of the Dialog component
 * @interface IDialogProps
 * @extends {IDialogState}
 */
export interface IDialogProps extends IDialogState {
  /**
   * Prop for group all the props of the Dialog component
   * @type {IDialogState}
   * @memberof IDialogProps
   */
  options?: IDialogState
}

/**
 * Class to define the Dialog component
 * @class Dialog
 * @extends {React.Component<IDialogProps, IDialogState>}
 */
declare class Dialog extends React.Component<IDialogProps, IDialogState> {
  /**
   * Reference of the PopupDialog
   * @type {PopupDialog}
   * @memberof Dialog
   */
  public root?: PopupDialog

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof Dialog
   */
  public render(): TypeComponent

  /**
   * Method that renders the Title component
   * @returns {TypeComponent}
   * @memberof Dialog
   */
  public Title(): TypeComponent

  /**
   * Method to show the dialog
   * @returns {void}
   * @memberof Dialog
   */
  public show(): void

  /**
   * Method to dismiss the dialog
   * @returns {void}
   * @memberof Dialog
   */
  public dismiss(): void

  /**
   * Method that fire when the dialog is dismissed
   * @private
   * @returns {void}
   * @memberof Dialog
   */
  private _onDismiss(): void

  /**
   * Method that fire when the dialog is showed
   * @private
   * @returns {void}
   * @memberof Dialog
   */
  private _onShow(): void

  /**
   * Method to get the animation when the dialog is showed
   * @private
   * @returns {(FadeAnimation | ScaleAnimation | SlideAnimation | undefined)}
   * @memberof Dialog
   */
  private _getAnimation(): FadeAnimation | ScaleAnimation | SlideAnimation | undefined

  /**
   * Method to process the props
   * @private
   * @returns {IDialogProps}
   * @memberof Dialog
   */
  private _processProps(): IDialogProps
}

export default Dialog

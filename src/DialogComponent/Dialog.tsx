import * as React from 'react'
import { StyleSheet } from 'react-native'
import PopupDialog, { FadeAnimation, ScaleAnimation, SlideAnimation } from 'react-native-popup-dialog'

import { AppHelper, TypeComponent } from '@ticmakers-react-native/core'
import DialogTitle from './../DialogTitle/DialogTitle'
import { IDialogProps, IDialogState } from './../../index'
import styles from './styles'

/**
 * Class to define the Dialog component
 * @class Dialog
 * @extends {React.Component<IDialogProps, IDialogState>}
 */
export default class Dialog extends React.Component<IDialogProps, IDialogState> {
  /**
   * Reference of the PopupDialog
   * @type {PopupDialog}
   * @memberof Dialog
   */
  public root?: PopupDialog

  /**
   * Creates an instance of Dialog.
   * @param {IDialogProps} props    An object of the DialogProps
   * @memberof Dialog
   */
  constructor(props: IDialogProps) {
    super(props)
    this.state = this._processProps()

    this.dismiss = this.dismiss.bind(this)
    this.show = this.show.bind(this)
    this._onDismiss = this._onDismiss.bind(this)
    // this._onHardwareBackPress = this._onHardwareBackPress.bind(this)
    // this._onPressOutside = this._onPressOutside.bind(this)
    this._onShow = this._onShow.bind(this)
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof Dialog
   */
  public render(): TypeComponent {
    const { animationDuration, containerStyle, dismissOnHardwareBackPress, dismissOnTouchOutside, footer, hasOverlay, height, overlayBackgroundColor, overlayOpacity, overlayPointerEvents, rounded, style, useNativeDriver, width } = this._processProps()
    const { children } = this.props
    const { visible } = this.state

    const props = {
      animationDuration,
      containerStyle,
      dismissOnHardwareBackPress,
      dismissOnTouchOutside,
      hasOverlay,
      height,
      overlayBackgroundColor,
      overlayOpacity,
      overlayPointerEvents,
      rounded,
      useNativeDriver,
      width,
      // tslint:disable-next-line: object-literal-sort-keys
      actions: footer,
      dialogAnimation: this._getAnimation(),
      dialogStyle: style,
      dialogTitle: this.Title(),
      onDismissed: this._onDismiss,
      // onHardwareBackPress: this._onHardwareBackPress as any,
      onShown: this._onShow,
      // onTouchOutside: this._onPressOutside,
      ref: (dialog: PopupDialog) => this.root = dialog,
      show: visible,
    }

    return (
      <PopupDialog { ...props as any }>
        { children }
      </PopupDialog>
    )
  }

  /**
   * Method that renders the Title component
   * @returns {TypeComponent}
   * @memberof Dialog
   */
  public Title(): TypeComponent {
    const { title, titleStyle } = this._processProps()
    const props = {
      containerStyle: StyleSheet.flatten([styles.titleContainer]),
      style: StyleSheet.flatten([styles.title, titleStyle]),
    }

    if (title) {
      if (AppHelper.isComponent(title)) {
        return React.cloneElement(title as any, props)
      }

      return <DialogTitle { ...props } title={ title as string } />
    }
  }

  /**
   * Method to show the dialog
   * @returns {void}
   * @memberof Dialog
   */
  public show(): void {
    if (this.root) {
      // this.root.show()
      this.setState({ visible: true })
    }
  }

  /**
   * Method to dismiss the dialog
   * @returns {void}
   * @memberof Dialog
   */
  public dismiss(): void {
    if (this.root) {
      // this.root.dismiss()
      this.setState({ visible: false })
    }
  }

  /**
   * Method that fire when the dialog is dismissed
   * @private
   * @returns {void}
   * @memberof Dialog
   */
  private _onDismiss(): void {
    const { onDismiss } = this._processProps()

    if (onDismiss) {
      return onDismiss()
    }
  }

  /**
   * Method that fire when the dialog is showed
   * @private
   * @returns {void}
   * @memberof Dialog
   */
  private _onShow(): void {
    const { onShow } = this._processProps()

    if (onShow) {
      return onShow()
    }
  }

  /**
   * Method to get the animation when the dialog is showed
   * @private
   * @returns {(FadeAnimation | ScaleAnimation | SlideAnimation | undefined)}
   * @memberof Dialog
   */
  private _getAnimation(): FadeAnimation | ScaleAnimation | SlideAnimation | undefined {
    const { animation, animationDuration, slideFrom, useNativeDriver } = this._processProps()
    const animationProps: any = {
      useNativeDriver,
      // tslint:disable-next-line: object-literal-sort-keys
      initialValue: 0,
    }

    switch (animation) {
      case 'fade': {
        return new FadeAnimation({ ...animationProps, animationDuration })
      }

      case 'scale': {
        return new ScaleAnimation(animationProps)
      }

      case 'slide': {
        return new SlideAnimation({ ...animationProps, slideFrom })
      }
    }
  }

  /**
   * Method to process the props
   * @private
   * @returns {IDialogProps}
   * @memberof Dialog
   */
  private _processProps(): IDialogProps {
    const { animation, animationDuration, containerStyle, dismissOnHardwareBackPress, dismissOnTouchOutside, footer, hasOverlay, height, onDismiss, onShow, options, overlayBackgroundColor, overlayOpacity, overlayPointerEvents, rounded, slideFrom, style, title, titleStyle, useNativeDriver, visible, width } = this.props

    const props: IDialogProps = {
      animation: (options && options.animation) || (animation || undefined),
      animationDuration: (options && options.animationDuration) || (animationDuration || 200),
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      dismissOnHardwareBackPress: (options && options.dismissOnHardwareBackPress) || (dismissOnHardwareBackPress || true),
      dismissOnTouchOutside: (options && options.dismissOnTouchOutside) || (dismissOnTouchOutside || true),
      footer: (options && options.footer) || (footer || undefined),
      hasOverlay: (options && options.hasOverlay) || (hasOverlay || true),
      height: (options && options.height) || (height || null),
      onDismiss: (options && options.onDismiss) || (onDismiss || undefined),
      onShow: (options && options.onShow) || (onShow || undefined),
      overlayBackgroundColor: (options && options.overlayBackgroundColor) || (overlayBackgroundColor || '#000'),
      overlayOpacity: (options && options.overlayOpacity) || (overlayOpacity || 0.7),
      overlayPointerEvents: (options && options.overlayPointerEvents) || (overlayPointerEvents || 'auto'),
      rounded: (options && options.rounded) || (rounded || false),
      slideFrom: (options && options.slideFrom) || (slideFrom || 'bottom'),
      style: (options && options.style) || (style || undefined),
      title: (options && options.title) || (title || undefined),
      titleStyle: (options && options.titleStyle) || (titleStyle || undefined),
      useNativeDriver: (options && options.useNativeDriver) || (useNativeDriver || true),
      visible: (options && options.visible) || (visible || false),
      width: (options && options.width) || (width || 0.90),
    }

    return props
  }
}

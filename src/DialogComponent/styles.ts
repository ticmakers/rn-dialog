import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    borderRadius: 0,
    elevation: 5,
    minHeight: 96,
    shadowColor: 'black',
    shadowOffset: {
      height: 2,
      width: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },

  title: {
  },
  titleContainer: {
  },
})

export default styles

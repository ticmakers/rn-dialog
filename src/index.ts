import Dialog from './DialogComponent/Dialog'
import DialogButton from './DialogButton/DialogButton'
import DialogContent from './DialogContent/DialogContent'
import DialogFooter from './DialogFooter/DialogFooter'
import DialogManager from './DialogManager/DialogManager'
import DialogTitle from './DialogTitle/DialogTitle'

export { Dialog, DialogButton, DialogContent, DialogFooter, DialogTitle }

export default new DialogManager()

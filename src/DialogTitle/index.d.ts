import * as React from 'react'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'

/**
 * Type to define the align position for the DialogTitle component
 */
export type TypeDialogTitleAlign = 'left' | 'center' | 'right'

/**
 * Interface to define the states of the DialogTitle component
 * @interface IDialogTitleState
 */
export interface IDialogTitleState {
  /**
   * Set a align value to the button (left|center|right)
   * @type {TypeDialogTitleAlign}
   * @memberof IDialogTitleState
   * @default left
   */
  align?: TypeDialogTitleAlign

  /**
   * Apply a custom style to the container of the dialog title
   * @type {TypeStyle}
   * @memberof IDialogTitleState
   */
  containerStyle?: TypeStyle

  /**
   * Set true to define a title bar
   * @type {boolean}
   * @memberof IDialogTitleState
   * @default true
   */
  hasTitleBar?: boolean

  /**
   * Apply a custom style to the dialog title
   * @type {TypeStyle}
   * @memberof IDialogTitleState
   */
  style?: TypeStyle

  /**
   * A string to define the title of the dialog
   * @type {string}
   * @memberof IDialogTitleState
   */
  title?: string
}

/**
 * Interface to define the props of the DialogTitle component
 * @interface IDialogTitleProps
 * @extends {IDialogTitleState}
 */
export interface IDialogTitleProps extends IDialogTitleState {
  /**
   * Prop for group all the props of the DialogTitle component
   * @type {IDialogTitleState}
   * @memberof IDialogTitleProps
   */
  options?: IDialogTitleState
}

/**
 * Class to define the component DialogTitle used in Dialog
 * @class DialogTitle
 * @extends {React.Component<IDialogTitleProps, IDialogTitleState>}
 */
declare class DialogTitle extends React.Component<IDialogTitleProps, IDialogTitleState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogTitle
   */
  public render(): TypeComponent

  /**
   * Method that process the props
   * @private
   * @returns {IDialogTitleState}
   * @memberof DialogTitle
   */
  private _processProps(): IDialogTitleState
}

export default DialogTitle

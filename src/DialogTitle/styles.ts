import { StyleSheet, Dimensions } from 'react-native'

const scale = Dimensions.get('window').width / 375

const normalize = (size: number): number => {
  return Math.round(scale * size)
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },

  title: {
    fontSize: normalize(17),
    lineHeight: normalize(23),
  },
})

export default styles

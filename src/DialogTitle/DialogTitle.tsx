import * as React from 'react'
import { StyleSheet } from 'react-native'
import { DialogTitle as PopupDialogTitle, DialogTitleProps } from 'react-native-popup-dialog'

import { TypeComponent } from '@ticmakers-react-native/core'
import { IDialogTitleProps, IDialogTitleState } from './../../index'
import styles from './styles'

/**
 * Class to define the component DialogTitle used in Dialog
 * @class DialogTitle
 * @extends {React.Component<IDialogTitleProps, IDialogTitleState>}
 */
export default class DialogTitle extends React.Component<IDialogTitleProps, IDialogTitleState> {
  /**
   * Creates an instance of DialogTitle.
   * @param {IDialogTitleProps} props     Props of the component
   * @memberof DialogTitle
   */
  constructor(props: IDialogTitleProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof DialogTitle
   */
  public render(): TypeComponent {
    const { align, containerStyle, hasTitleBar, style, title } = this._processProps()

    const props: DialogTitleProps = {
      align,
      hasTitleBar,
      // tslint:disable-next-line: object-literal-sort-keys
      style: StyleSheet.flatten([styles.container, containerStyle]),
      textStyle: StyleSheet.flatten([styles.title, style]),
      title: title as string,
    }

    return <PopupDialogTitle { ...props } />
  }

  /**
   * Method that process the props
   * @private
   * @returns {IDialogTitleState}
   * @memberof DialogTitle
   */
  private _processProps(): IDialogTitleState {
    const { align, containerStyle, hasTitleBar, options, style, title } = this.props

    const props: IDialogTitleState = {
      align: (options && options.align) || (align || 'center'),
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      hasTitleBar: (options && options.hasTitleBar) || (hasTitleBar || true),
      style: (options && options.style) || (style || undefined),
      title: (options && options.title) || (title || undefined),
    }

    return props
  }
}

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var react_native_popup_dialog_1 = require("react-native-popup-dialog");
var styles_1 = require("./styles");
var DialogButton = (function (_super) {
    __extends(DialogButton, _super);
    function DialogButton(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        return _this;
    }
    DialogButton.prototype.render = function () {
        var _a = this._processProps(), activeOpacity = _a.activeOpacity, align = _a.align, bordered = _a.bordered, disabled = _a.disabled, style = _a.style, title = _a.title, titleStyle = _a.titleStyle;
        var props = {
            activeOpacity: activeOpacity,
            align: align,
            bordered: bordered,
            disabled: disabled,
            onPress: this._onPress.bind(this),
            style: react_native_1.StyleSheet.flatten([styles_1.default.container, style]),
            text: title,
            textStyle: react_native_1.StyleSheet.flatten([styles_1.default.title, titleStyle]),
        };
        return React.createElement(react_native_popup_dialog_1.DialogButton, __assign({}, props));
    };
    DialogButton.prototype._onPress = function () {
        var onPress = this._processProps().onPress;
        if (onPress) {
            return onPress();
        }
    };
    DialogButton.prototype._processProps = function () {
        var _a = this.props, activeOpacity = _a.activeOpacity, align = _a.align, bordered = _a.bordered, disabled = _a.disabled, onPress = _a.onPress, options = _a.options, style = _a.style, title = _a.title, titleStyle = _a.titleStyle;
        var props = {
            activeOpacity: (options && options.activeOpacity) || (activeOpacity || 0.6),
            align: (options && options.align) || (align || 'center'),
            bordered: (options && options.bordered) || (bordered || false),
            disabled: (options && options.disabled) || (disabled || false),
            onPress: (options && options.onPress) || (onPress || undefined),
            style: (options && options.style) || (style || undefined),
            title: (options && options.title) || (title || undefined),
            titleStyle: (options && options.titleStyle) || (titleStyle || undefined),
        };
        return props;
    };
    return DialogButton;
}(React.Component));
exports.default = DialogButton;
//# sourceMappingURL=DialogButton.js.map
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var styles_1 = require("./styles");
var DialogContent = (function (_super) {
    __extends(DialogContent, _super);
    function DialogContent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        return _this;
    }
    DialogContent.prototype.render = function () {
        var children = this.props.children;
        var style = this._processProps().style;
        var props = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.container, style]),
        };
        return React.createElement(react_native_1.View, __assign({}, props), children);
    };
    DialogContent.prototype._processProps = function () {
        var _a = this.props, options = _a.options, style = _a.style;
        var props = {
            style: (options && options.style) || (style || undefined),
        };
        return props;
    };
    return DialogContent;
}(React.Component));
exports.default = DialogContent;
//# sourceMappingURL=DialogContent.js.map
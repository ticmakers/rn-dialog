"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var styles_1 = require("./styles");
var DialogFooter = (function (_super) {
    __extends(DialogFooter, _super);
    function DialogFooter(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        return _this;
    }
    DialogFooter.prototype.render = function () {
        var _a = this._processProps(), bordered = _a.bordered, style = _a.style;
        var children = this.props.children;
        var containerStyle = children.length > 2 ? styles_1.default.actionsVertical : styles_1.default.actionsHorizontal;
        var content = children;
        if (children.length === 2) {
            content = React.Children.map(children, function (child, index) { return React.cloneElement(child, { bordered: (1 % index === 0 && bordered) }); });
        }
        var props = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.container, containerStyle, bordered && styles_1.default.border, style]),
        };
        return React.createElement(react_native_1.View, __assign({}, props), content);
    };
    DialogFooter.prototype._processProps = function () {
        var _a = this.props, bordered = _a.bordered, options = _a.options, style = _a.style;
        var props = {
            bordered: (options && options.bordered) || (bordered || true),
            style: (options && options.style) || (style || undefined),
        };
        return props;
    };
    return DialogFooter;
}(React.Component));
exports.default = DialogFooter;
//# sourceMappingURL=DialogFooter.js.map
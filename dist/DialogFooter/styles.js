"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var styles = react_native_1.StyleSheet.create({
    container: {},
    border: {
        borderColor: '#CCD0D5',
        borderTopWidth: 1 / react_native_1.PixelRatio.get(),
    },
    actionsVertical: {
        flexDirection: 'column',
        height: 200,
    },
    actionsHorizontal: {
        flexDirection: 'row',
    },
});
exports.default = styles;
//# sourceMappingURL=styles.js.map
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var react_native_popup_dialog_1 = require("react-native-popup-dialog");
var core_1 = require("@ticmakers-react-native/core");
var DialogTitle_1 = require("./../DialogTitle/DialogTitle");
var styles_1 = require("./styles");
var Dialog = (function (_super) {
    __extends(Dialog, _super);
    function Dialog(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        _this.dismiss = _this.dismiss.bind(_this);
        _this.show = _this.show.bind(_this);
        _this._onDismiss = _this._onDismiss.bind(_this);
        _this._onShow = _this._onShow.bind(_this);
        return _this;
    }
    Dialog.prototype.render = function () {
        var _this = this;
        var _a = this._processProps(), animationDuration = _a.animationDuration, containerStyle = _a.containerStyle, dismissOnHardwareBackPress = _a.dismissOnHardwareBackPress, dismissOnTouchOutside = _a.dismissOnTouchOutside, footer = _a.footer, hasOverlay = _a.hasOverlay, height = _a.height, overlayBackgroundColor = _a.overlayBackgroundColor, overlayOpacity = _a.overlayOpacity, overlayPointerEvents = _a.overlayPointerEvents, rounded = _a.rounded, style = _a.style, useNativeDriver = _a.useNativeDriver, width = _a.width;
        var children = this.props.children;
        var visible = this.state.visible;
        var props = {
            animationDuration: animationDuration,
            containerStyle: containerStyle,
            dismissOnHardwareBackPress: dismissOnHardwareBackPress,
            dismissOnTouchOutside: dismissOnTouchOutside,
            hasOverlay: hasOverlay,
            height: height,
            overlayBackgroundColor: overlayBackgroundColor,
            overlayOpacity: overlayOpacity,
            overlayPointerEvents: overlayPointerEvents,
            rounded: rounded,
            useNativeDriver: useNativeDriver,
            width: width,
            actions: footer,
            dialogAnimation: this._getAnimation(),
            dialogStyle: style,
            dialogTitle: this.Title(),
            onDismissed: this._onDismiss,
            onShown: this._onShow,
            ref: function (dialog) { return _this.root = dialog; },
            show: visible,
        };
        return (React.createElement(react_native_popup_dialog_1.default, __assign({}, props), children));
    };
    Dialog.prototype.Title = function () {
        var _a = this._processProps(), title = _a.title, titleStyle = _a.titleStyle;
        var props = {
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.titleContainer]),
            style: react_native_1.StyleSheet.flatten([styles_1.default.title, titleStyle]),
        };
        if (title) {
            if (core_1.AppHelper.isComponent(title)) {
                return React.cloneElement(title, props);
            }
            return React.createElement(DialogTitle_1.default, __assign({}, props, { title: title }));
        }
    };
    Dialog.prototype.show = function () {
        if (this.root) {
            this.setState({ visible: true });
        }
    };
    Dialog.prototype.dismiss = function () {
        if (this.root) {
            this.setState({ visible: false });
        }
    };
    Dialog.prototype._onDismiss = function () {
        var onDismiss = this._processProps().onDismiss;
        if (onDismiss) {
            return onDismiss();
        }
    };
    Dialog.prototype._onShow = function () {
        var onShow = this._processProps().onShow;
        if (onShow) {
            return onShow();
        }
    };
    Dialog.prototype._getAnimation = function () {
        var _a = this._processProps(), animation = _a.animation, animationDuration = _a.animationDuration, slideFrom = _a.slideFrom, useNativeDriver = _a.useNativeDriver;
        var animationProps = {
            useNativeDriver: useNativeDriver,
            initialValue: 0,
        };
        switch (animation) {
            case 'fade': {
                return new react_native_popup_dialog_1.FadeAnimation(__assign(__assign({}, animationProps), { animationDuration: animationDuration }));
            }
            case 'scale': {
                return new react_native_popup_dialog_1.ScaleAnimation(animationProps);
            }
            case 'slide': {
                return new react_native_popup_dialog_1.SlideAnimation(__assign(__assign({}, animationProps), { slideFrom: slideFrom }));
            }
        }
    };
    Dialog.prototype._processProps = function () {
        var _a = this.props, animation = _a.animation, animationDuration = _a.animationDuration, containerStyle = _a.containerStyle, dismissOnHardwareBackPress = _a.dismissOnHardwareBackPress, dismissOnTouchOutside = _a.dismissOnTouchOutside, footer = _a.footer, hasOverlay = _a.hasOverlay, height = _a.height, onDismiss = _a.onDismiss, onShow = _a.onShow, options = _a.options, overlayBackgroundColor = _a.overlayBackgroundColor, overlayOpacity = _a.overlayOpacity, overlayPointerEvents = _a.overlayPointerEvents, rounded = _a.rounded, slideFrom = _a.slideFrom, style = _a.style, title = _a.title, titleStyle = _a.titleStyle, useNativeDriver = _a.useNativeDriver, visible = _a.visible, width = _a.width;
        var props = {
            animation: (options && options.animation) || (animation || undefined),
            animationDuration: (options && options.animationDuration) || (animationDuration || 200),
            containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
            dismissOnHardwareBackPress: (options && options.dismissOnHardwareBackPress) || (dismissOnHardwareBackPress || true),
            dismissOnTouchOutside: (options && options.dismissOnTouchOutside) || (dismissOnTouchOutside || true),
            footer: (options && options.footer) || (footer || undefined),
            hasOverlay: (options && options.hasOverlay) || (hasOverlay || true),
            height: (options && options.height) || (height || null),
            onDismiss: (options && options.onDismiss) || (onDismiss || undefined),
            onShow: (options && options.onShow) || (onShow || undefined),
            overlayBackgroundColor: (options && options.overlayBackgroundColor) || (overlayBackgroundColor || '#000'),
            overlayOpacity: (options && options.overlayOpacity) || (overlayOpacity || 0.7),
            overlayPointerEvents: (options && options.overlayPointerEvents) || (overlayPointerEvents || 'auto'),
            rounded: (options && options.rounded) || (rounded || false),
            slideFrom: (options && options.slideFrom) || (slideFrom || 'bottom'),
            style: (options && options.style) || (style || undefined),
            title: (options && options.title) || (title || undefined),
            titleStyle: (options && options.titleStyle) || (titleStyle || undefined),
            useNativeDriver: (options && options.useNativeDriver) || (useNativeDriver || true),
            visible: (options && options.visible) || (visible || false),
            width: (options && options.width) || (width || 0.90),
        };
        return props;
    };
    return Dialog;
}(React.Component));
exports.default = Dialog;
//# sourceMappingURL=Dialog.js.map
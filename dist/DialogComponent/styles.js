"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var styles = react_native_1.StyleSheet.create({
    container: {
        borderRadius: 0,
        elevation: 5,
        minHeight: 96,
        shadowColor: 'black',
        shadowOffset: {
            height: 2,
            width: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 5,
    },
    title: {},
    titleContainer: {},
});
exports.default = styles;
//# sourceMappingURL=styles.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Dialog_1 = require("./DialogComponent/Dialog");
exports.Dialog = Dialog_1.default;
var DialogButton_1 = require("./DialogButton/DialogButton");
exports.DialogButton = DialogButton_1.default;
var DialogContent_1 = require("./DialogContent/DialogContent");
exports.DialogContent = DialogContent_1.default;
var DialogFooter_1 = require("./DialogFooter/DialogFooter");
exports.DialogFooter = DialogFooter_1.default;
var DialogManager_1 = require("./DialogManager/DialogManager");
var DialogTitle_1 = require("./DialogTitle/DialogTitle");
exports.DialogTitle = DialogTitle_1.default;
exports.default = new DialogManager_1.default();
//# sourceMappingURL=index.js.map
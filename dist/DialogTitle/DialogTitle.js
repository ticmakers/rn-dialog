"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var react_native_popup_dialog_1 = require("react-native-popup-dialog");
var styles_1 = require("./styles");
var DialogTitle = (function (_super) {
    __extends(DialogTitle, _super);
    function DialogTitle(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        return _this;
    }
    DialogTitle.prototype.render = function () {
        var _a = this._processProps(), align = _a.align, containerStyle = _a.containerStyle, hasTitleBar = _a.hasTitleBar, style = _a.style, title = _a.title;
        var props = {
            align: align,
            hasTitleBar: hasTitleBar,
            style: react_native_1.StyleSheet.flatten([styles_1.default.container, containerStyle]),
            textStyle: react_native_1.StyleSheet.flatten([styles_1.default.title, style]),
            title: title,
        };
        return React.createElement(react_native_popup_dialog_1.DialogTitle, __assign({}, props));
    };
    DialogTitle.prototype._processProps = function () {
        var _a = this.props, align = _a.align, containerStyle = _a.containerStyle, hasTitleBar = _a.hasTitleBar, options = _a.options, style = _a.style, title = _a.title;
        var props = {
            align: (options && options.align) || (align || 'center'),
            containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
            hasTitleBar: (options && options.hasTitleBar) || (hasTitleBar || true),
            style: (options && options.style) || (style || undefined),
            title: (options && options.title) || (title || undefined),
        };
        return props;
    };
    return DialogTitle;
}(React.Component));
exports.default = DialogTitle;
//# sourceMappingURL=DialogTitle.js.map
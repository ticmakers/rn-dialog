"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var scale = react_native_1.Dimensions.get('window').width / 375;
var normalize = function (size) {
    return Math.round(scale * size);
};
var styles = react_native_1.StyleSheet.create({
    container: {
        padding: 20,
    },
    title: {
        fontSize: normalize(17),
        lineHeight: normalize(23),
    },
});
exports.default = styles;
//# sourceMappingURL=styles.js.map
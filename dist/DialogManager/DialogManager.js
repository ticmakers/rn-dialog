"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_root_siblings_1 = require("react-native-root-siblings");
var Dialog_1 = require("./../DialogComponent/Dialog");
var DialogManager = (function () {
    function DialogManager() {
        this.dialogs = [];
        this.destroyTimeout = 150;
    }
    Object.defineProperty(DialogManager.prototype, "currentDialog", {
        get: function () {
            return this.dialogs[this.dialogs.length - 1];
        },
        enumerable: true,
        configurable: true
    });
    DialogManager.prototype.add = function (props, callback) {
        var _this = this;
        var _props = __assign(__assign({}, props), { onDismiss: function () { return _this.onDialogDismissed(props.onDismiss); } });
        var dialog = new react_native_root_siblings_1.default(React.createElement(Dialog_1.default, __assign({}, _props)), callback);
        this.dialogs.push(dialog);
    };
    DialogManager.prototype.destroy = function () {
        var dialog = this.dialogs.pop();
        setTimeout(function () { return dialog && dialog.destroy(); }, this.destroyTimeout);
    };
    DialogManager.prototype.onDialogDismissed = function (onDismiss) {
        if (onDismiss) {
            onDismiss();
        }
        this.destroy();
    };
    DialogManager.prototype.update = function (props, callback) {
        var _this = this;
        var _props = __assign(__assign({}, props), { onDismiss: function () { return _this.onDialogDismissed(props.onDismiss); } });
        this.currentDialog.update(React.createElement(Dialog_1.default, __assign({}, _props)), callback);
    };
    DialogManager.prototype.show = function (props, callback) {
        this.add(__assign(__assign({}, props), { visible: true }), callback);
    };
    DialogManager.prototype.dismiss = function (callback) {
        this.update({ visible: false }, callback);
    };
    DialogManager.prototype.dismissAll = function (callback) {
        var _this = this;
        this.dialogs.forEach(function () {
            _this.dismiss(callback);
        });
    };
    return DialogManager;
}());
exports.default = DialogManager;
//# sourceMappingURL=DialogManager.js.map
import * as React from 'react'
import { ViewStyle, RegisteredStyle, RecursiveArray, StyleProp } from 'react-native'

import Dialog, { IDialogProps, IDialogState, TypeDialogAnimation } from './src/DialogComponent'
import DialogButton, { IDialogButtonProps, IDialogButtonState } from './src/DialogButton'
import DialogContent, { IDialogContentProps, IDialogContentState } from './src/DialogContent'
import DialogFooter, { IDialogFooterProps, IDialogFooterState } from './src/DialogFooter'
import DialogTitle, { IDialogTitleProps, IDialogTitleState, TypeDialogTitleAlign } from './src/DialogTitle'
import DialogManager from './src/index'

/**
 * Declaration to define the module
 */
declare module '@ticmakers-react-native/dialog'

/**
 * Export components, declarations and types
 */
export {
  Dialog,
  IDialogProps,
  IDialogState,
  TypeDialogAnimation,

  DialogButton,
  IDialogButtonProps,
  IDialogButtonState,

  DialogContent,
  IDialogContentProps,
  IDialogContentState,

  DialogFooter,
  IDialogFooterProps,
  IDialogFooterState,

  DialogTitle,
  IDialogTitleProps,
  IDialogTitleState,
  TypeDialogTitleAlign,
}

export default DialogManager

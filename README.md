# TIC Makers - React Native Dialog
React native component for dialog.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Dialog Expo's snack]()

## Install

Install `@ticmakers-react-native/dialog` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/dialog --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/dialog
```

## How to use?

```javascript
import React from 'react'
import Dialog from '@ticmakers-react-native/dialog'

export default class App extends React.Component {

  render() { }
}
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| ---- | ---- | ---- | ----

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Create tests
- Add new props and components in readme
- Improve README

## Version 1.0.2 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-dialog/src/master/CHANGELOG.md
